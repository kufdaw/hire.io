require('./bootstrap');

import { InertiaApp } from '@inertiajs/inertia-vue'
import Vue from 'vue'
import vuetify from './vuetify'
import store from './store/store'
import VuetifyConfirm from 'vuetify-confirm'
import VueI18n from 'vue-i18n';
import './filters'
import pl from './lang/pl.json'
import en from './lang/en.json'

const moment = require('moment')
require('moment/locale/pl')

Vue.use(VueI18n)

const messages = {
    pl: pl,
    en: en
};

const i18n = new VueI18n({
    locale: 'pl',
    messages
});

Vue.use(require('vue-moment'), {
    moment
})

Vue.use(VuetifyConfirm, {
    vuetify,
    buttonTrueText: i18n.tc('subtitle.confirm'),
    buttonFalseText: i18n.tc('subtitle.refuse'),
    // color: 'warning',
    // icon: 'warning',
    // title: 'Warning',
    // width: 350,
    // property: '$confirm'
})

Vue.use(InertiaApp)

axios.defaults.headers.common = {
    'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
};

Vue.prototype.$route = (...args) => route(...args).url()
const app = document.getElementById('app')

new Vue({
    vuetify,
    store,
    i18n,
    render: h => h(InertiaApp, {
        props: {
            initialPage: JSON.parse(app.dataset.page),
            resolveComponent: name => import(`@/Pages/${name}`).then(module => module.default),
        },
    }),
}).$mount(app)
