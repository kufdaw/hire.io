import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify)

const opts = {
    breakpoints: {
        thresholds: {
            xs: 476,
            sm: 640,
            md: 768,
            lg: 1024,
            xl: 1280
        }
    },
    icons: {
        iconfont: 'mdi',
    },
    theme: {
        themes: {
            dark: {
                primary: '#336ecc',
                secondary: '#50b5ff',
                accent: '#3dd598',
                error: '#fc5a5a',
                info: '#336ecc',
                success: '#82c43c',
                warning: '#ffc542',
            },
            light: {
                primary: '#336ecc',
                secondary: '#50b5ff',
                accent: '#3dd598',
                error: '#fc5a5a',
                info: '#336ecc',
                success: '#82c43c',
                warning: '#ffc542',
            }
        }
    }
}

export default new Vuetify(opts)
