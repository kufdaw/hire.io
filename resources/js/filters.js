import Vue from 'vue'

Vue.filter('initials', function (string) {
    const words = string.split(/[\s-]+/)
    let initials = ''

    for (let i = 0; i < words.length; i++) {
        initials += words[i].substr(0, 1).toUpperCase()
    }

    if (initials.length > 3) {
        initials = initials.substr(0, 3)
    }
    return initials
})

Vue.filter('truncate', function (text, length) {
    if (text.length < length) {
        return text
    }

    return text.substring(0, length) + '...';
});

Vue.filter('candidateAdStatus', string => {
    switch (string) {
        case 'requested':
            return 'Wysłany';
        case 'opened':
            return 'Otworzony'
        case 'rejected':
            return 'Odrzucony'
        case 'proceeded':
            return 'Procedowany'
        default:
            return string
    }
})

Vue.filter('role', string => {
    switch (string) {
        case 'company':
            return 'Pracodawca';
        case 'agency':
            return 'Agencja'
        case 'moderator':
            return 'Moderator'
        case 'super-admin':
            return 'Administrator'
        default:
            return ''
    }
})
