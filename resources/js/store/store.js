import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store( {
    state: {
        navigationOpen: true
    },
    getters: {
        navigationOpen: state => state.navigationOpen
    },
    mutations: {
        toggleNavigation(state) {
            state.navigationOpen = !state.navigationOpen
        },
        changeNavigationOpen(state, { status }) {
            state.navigationOpen = status
        }
    },
})