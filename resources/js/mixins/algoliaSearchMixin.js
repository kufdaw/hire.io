export default {
    data() {
        return {
            preferredLanguage: 'pl-PL'
        }
    },
    methods: {
        searchPlaces(text) {
            return new Promise( (resolve, reject) => {
                axios.post(route('search-region'), { text }).then( response => {
                    resolve(response.data)
                }).catch( error => {
                    reject(error)
                })
            })
        },
        searchCitiesAround(location_id, radius) {
            return new Promise( (resolve, reject) => {
                axios.post(route('search-region-around'), { location_id, radius }).then( response => {
                    resolve(response.data)
                }).catch( error => {
                    reject(error)
                })
            })
        },
    },
}
