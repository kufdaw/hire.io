<!DOCTYPE html>
<html style="overflow: hidden">
    <head>
        @routes
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel='favicon' type='image/x-icon' href="{{ asset('images/favicon.ico') }}" />
        <script src="https://js.stripe.com/v3/"></script>
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet" />
        <script src="{{ mix('/js/app.js') }}" defer></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174361582-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-174361582-1');
        </script>
    </head>
    <body>
        @inertia
    </body>
</html>
