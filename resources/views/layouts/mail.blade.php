<div style="position: relative; font-family: Poppins, sans-serif;">
    <div>
        <img style="width: 220px; position: absolute; top: 0; right: 0;" src="{{ asset('/images/emplostore-logo.png') }}" />
    </div>
    @yield('content')

</div>
