@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        <strong>Dostałeś email od:</strong> {{ $emailSender }}
        <br><br>
        <strong>Treść maila to:</strong>
        <br>
            {{ $emailMessage }}
        <br>
    </div>

    @include('mail.includes.footer')
@endsection
