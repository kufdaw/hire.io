@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        Wysłano zapytanie dotyczące kandydata {{ $candidateName }}.
        <br>
        Aby zobaczyć, kliknij <a href="{{ $link }}">tutaj</a>...
    </div>

    @include('mail.includes.footer')
@endsection
