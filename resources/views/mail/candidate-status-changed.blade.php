@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        Pracodawca zmienił status kandydata {{ $candidateName }}.
        <br>
        Aby się zalogować i sprawdzić kliknij<a href="{{ $link }}">tutaj</a>...
    </div>

    @include('mail.includes.footer')
@endsection
