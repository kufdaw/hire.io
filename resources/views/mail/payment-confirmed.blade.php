@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        Uprzejmie informujemy, że otrzymaliśmy Twoją płatność. Zakupione usługi zostały włączone.
        <br>
    </div>

    @include('mail.includes.footer')
@endsection
