@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        Masz nową wiadomość w serwisie.
        <br>
        Aby ją zobaczyć, kliknij <a href="{{ $link }}">tutaj</a>...
    </div>

    @include('mail.includes.footer')
@endsection
