<div>
    <br>
    Pozdrawiamy, <br>
    Zespól emplostore.com
</div>
<div style="border-top: 1px solid #ccc; margin: 16px 0; color: #999; width: 100%; text-align: center; padding-top: 24px; font-size: 12px;">

    Wysłaliśmy tę wiadomość, ponieważ Twój e-mail został zapisany w portalu emplostore.com. Prosimy nie odpowiadać na tę wiadomość. Jeśli masz jakieś pytania odwiedź naszą stronę internetową.
    <br>
    <br>
    RB Solutions Sp. z o.o. zarejestrowana w Polsce pod nr NIP 676-248-94-16<br>
    ul. Krupnicza 12/7, 31-123 Kraków
    <br>
    <br>
    Jeżeli chcesz się wypisać lub zmienić ustawienia mailingu kliknij <a href="<?php echo route('dashboard-notifications-settings') ?>">tutaj</a>.
</div>
