@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        Profil Twojego kandydata stracił ważność i nie wyświetla się już. Aby go włączyć na kolejne 21 dni, kliknij <a href="{{ $link }}">tutaj</a>.
    </div>

    @include('mail.includes.footer')
@endsection
