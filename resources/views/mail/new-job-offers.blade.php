@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        <br>
        Pojawiły się nowe zlecenia rekrutacji, które mogą Cię zainteresować:
    </div>
    <div style="display: flex; flex-direction: column; margin-top: 48px;">
        @foreach ($ads as $ad)
            <div style="display: flex; margin: 12px 0">
                <div style="flex-basis: 250px;">
                    {{ $ad->title }}
                </div>
                <div style="flex-basis: 250px;">
                    {{ $ad->region }}
                </div>
                <div style="flex-basis: 250px;">
                    <a href="{{ route('ad', $ad->id) }}">Zobacz profil</a>
                </div>
            </div>
        @endforeach

        <div style="margin-top: 36px;">
            Aby zobaczyć wszystkie nowe zlecenia, <a href="{{ route('ads') }}">kliknij tutaj</a>.
        </div>
    </div>

    @include('mail.includes.footer')
@endsection
