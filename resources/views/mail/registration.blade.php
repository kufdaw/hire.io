@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        <br>

        Dokończ rejestrację, aktywując konto.
    <br>
        Kliknij w poniższy przycisk, aby aktywować
        <br>
        <a href="{{ $registrationLink }}">Aktywuj Konto</a>
    </div>

    @include('mail.includes.footer')
@endsection
