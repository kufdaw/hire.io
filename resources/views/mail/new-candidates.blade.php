@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        <br>
        Pojawili się nowi kandydaci, którzy mogą Cię zainteresować:
    </div>
    <div style="display: flex; flex-direction: column; margin-top: 48px;">
        @foreach ($candidates as $candidate)
            <div style="display: flex; margin: 12px 0">
                <div style="flex-basis: 250px;">
                    {{ $candidate->title }}
                </div>
                <div style="flex-basis: 250px;">
                    {{ $candidate->region }}
                </div>
                <div style="flex-basis: 250px;">
                    <a href="{{ route('candidate', $candidate->id) }}">Zobacz profil</a>
                </div>
            </div>
        @endforeach

        <div style="margin-top: 36px;">
            Aby zobaczyć wszystkich ostatnich kandydatów, <a href="{{ route('candidates') }}">kliknij tutaj</a>.
        </div>
    </div>

    @include('mail.includes.footer')
@endsection
