@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        Twoje zlecenie wygasło. Jeśli chcesz je przedłużyć, kliknij <a href="{{ $link }}">tutaj</a>.
    </div>

    @include('mail.includes.footer')
@endsection
