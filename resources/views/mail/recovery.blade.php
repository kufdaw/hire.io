@extends('layouts.mail')
@section('content')
    @include('mail.includes.header')
    <div>
        <br><br>
        Aby zmienić swoje hasło kliknij w poniższy link:
        <br>
        <a href="{{ $recoveryLink }}">{{ $recoveryLink }}</a>
    </div>

    @include('mail.includes.footer')
@endsection
