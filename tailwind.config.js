module.exports = {
    prefix: 'tw-',
    important: true,
    corePlugins: {
        outline: false,
    },
    theme: {
        screens: {
            'sm': '600px',
            // => @media (min-width: 640px) { ... }

            'md': '960px',
            // => @media (min-width: 768px) { ... }

            'lg': '1264px',
            // => @media (min-width: 1024px) { ... }

            'xl': '1904px',
            // => @media (min-width: 1280px) { ... }
        },
        fontFamily: {
            display: ['Poppins', 'sans-serif'],
            body: ['Poppins', 'sans-serif'],
            extra: ['DM Sans', 'sans']
        },
        extend: {
            screens: {
                '2xl': '1440px'
            },
            colors: {
                'square-blue': '#336ecc',
                'square-blue-desaturated': '#336ecc',
                'square-cyan': '#50b5ff',
                'square-yellow': '#ffc542',
                'square-aquamarine': '#3dd598',
                'square-orange': '#ff974a',
                'square-red': '#fc5a5a',
                'square-green': '#82c43c',
                'square-violet': '#a461d8',
                'square-pink': '#ff9ad5',
                'square-black': '#171725',
                'square-black-dark': '#1c1c24',
                'square-black-darker': '#13131a',
                'square-grey-darkest': '#28282f',
                'square-grey-darker': '#44444f',
                'square-grey-dark': '#696974',
                'square-grey': '#92929d',
                'square-grey-light': '#b5b5be',
                'square-grey-lighter': '#d5d5dc',
                'square-grey-lightest': '#e2e2ea',
                'square-white-dark': '#f1f1f5',
                'square-white': '#fafafb'
            },
            margin: {
                '2px': '2px',
                '-2px': '-2px',
                '96': '24rem',
                '128': '32rem',
            },
            borderRadius: {
                'xl': '1rem',
                '2xl': '2rem'
            },
            height: {
                '14': '3.5rem'
            },
            width: {
                '14': '3.5rem'
            },
            fontSize: {
                '2xs': '11px',
                '3xs': '10px'
            },
            minHeight: {
                '10': '3rem',
                '64': '16rem'
            },
            maxHeight: {
                '10': '3rem',
                '64': '16rem'
            },
            minWidth: {
                '10': '3rem',
                '64': '16rem'
            },
            maxWidth: {
                '10': '3rem',
                '64': '16rem'
            },
        }
    },
    variants: {
        opacity: ['responsive', 'hover']
    }
}
