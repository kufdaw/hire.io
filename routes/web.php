<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AdsController;
use App\Http\Controllers\AgenciesController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\CandidatesController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ConversationsController;
use App\Http\Controllers\Dashboard\Admin\DashboardAdminAdsController;
use App\Http\Controllers\Dashboard\Admin\DashboardAdminCandidatesController;
use App\Http\Controllers\Dashboard\Admin\DashboardAdminConversationsController;
use App\Http\Controllers\Dashboard\DashboardAdsController;
use App\Http\Controllers\Dashboard\DashboardCandidatesController;
use App\Http\Controllers\Dashboard\DashboardPaymentsController;
use App\Http\Controllers\Dashboard\DashboardSiteSettingsController;
use App\Http\Controllers\Dashboard\DashboardUsersController;
use App\Http\Controllers\Dashboard\JobTypesController;
use App\Http\Controllers\Dashboard\LanguagesController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\RegionsController;
use App\Http\Controllers\TagsController;
use Inertia\Inertia;

Route::middleware(['web'])->group(function () {
    Route::middleware(['guest'])->group(function () {
        Route::get('join', [LoginController::class, 'indexJoin'])->name('join');
        Route::post('register', [RegisterController::class, 'create'])->name('register');
        Route::post('login', [LoginController::class, 'login'])->name('login');
        Route::post('recover-account', [ResetPasswordController::class, 'recover'])->name('recover-account');
        Route::get('recover-account/{string}', [ResetPasswordController::class, 'resetPassword'])->name('reset-password');
        Route::post('reset-password', [ResetPasswordController::class, 'confirmResetPassword'])->name('confirm-reset-password');
        Route::get('confirm-registration/{string}', [RegisterController::class, 'confirmRegistration'])->name('confirm-registration');
    });

    Route::get('rules', function() {
        return Inertia::render('Rules');
    })->name('rules');

    Route::get('privacy-policy', function() {
        return Inertia::render('PrivacyPolicy');
    })->name('privacy-policy');

    Route::get('logout', [LoginController::class, 'logout'])->name('logout');

    Route::get('', function () {
        return Inertia::render('Index');
    })->name('index');

    Route::get('for-agencies', function () {
        return Inertia::render('ForAgencies');
    })->name('for-agencies');


    Route::post('search-region', [RegionsController::class, 'search'])->name('search-region');
    Route::post('search-region-around', [RegionsController::class, 'searchAround'])->name('search-region-around');

    Route::get('candidates', [CandidatesController::class, 'index'])->name('candidates');
    Route::get('candidates/{candidateId}', [CandidatesController::class, 'fetch'])->name('candidate');

    Route::get('agencies', [AgenciesController::class, 'index'])->name('agencies');
    Route::post('agencies/paginate', [AgenciesController::class, 'getPaginated'])->name('get-paginated-agencies');
    Route::get('agencies/{agency}', [AgenciesController::class, 'fetch'])->name('agency');
    Route::post('agencies/{agency}/paginate', [AgenciesController::class, 'getPaginatedAgencyCandidates'])->name('get-paginated-agency-candidates');
    Route::post('search/candidates', [CandidatesController::class, 'search'])->name('search-candidates');
    Route::post('search/ads', [AdsController::class, 'search'])->name('search-ads');
    Route::get('contact', [ContactController::class, 'index'])->name('contact');
    Route::post('contact', [ContactController::class, 'sendContactEmail'])->name('send-contact-email');

    Route::middleware(['auth'])->group(function () {
        Route::prefix('ads')->group(function () {
            Route::get('', [AdsController::class, 'index'])->name('ads');
            Route::get('{ad}', [AdsController::class, 'fetch'])->name('ad');
            Route::post('{ad}/propose-candidate', [AdsController::class, 'proposeCandidate'])->name('propose-candidate')->middleware(['can:propose ad candidate']);
            Route::post('{ad}/remove-candidate/{candidate}', [AdsController::class, 'removeCandidate'])->name('remove-candidate')->middleware(['can:remove ad candidate']);
        });

        Route::post('conversations', [ConversationsController::class, 'create'])->name('create-conversation')->middleware(['can:create conversation']);

        Route::prefix('dashboard')->group(function () {
            Route::get('site-settings', [DashboardSiteSettingsController::class, 'index'])->name('dashboard-site-settings')->middleware(['can:manage site settings']);

            Route::put('job-types', [JobTypesController::class, 'create'])->name('create-job-type')->middleware(['can:create job type']);
            Route::delete('job-types/{jobType}', [JobTypesController::class, 'delete'])->name('delete-job-type')->middleware(['can:delete job type']);

            Route::put('languages', [LanguagesController::class, 'create'])->name('create-language')->middleware(['can:create language']);
            Route::delete('languages/{language}', [LanguagesController::class, 'delete'])->name('delete-language')->middleware(['can:delete language']);

            Route::get('payments', [DashboardPaymentsController::class, 'index'])->name('dashboard-payments')->middleware(['can:manage admin payments']);
            Route::post('payments/paginate', [DashboardPaymentsController::class, 'getPaginated'])->name('get-paginated-payments')->middleware(['can:manage admin payments']);

            Route::get('settings', [DashboardUsersController::class, 'globalSettings'])->name('dashboard-settings');
            Route::post('settings', [DashboardUsersController::class, 'updateGlobalSettings'])->name('update-global-settings');
            Route::post('settings/password', [DashboardUsersController::class, 'updatePassword'])->name('update-password');

            Route::get('settings/notifications', [DashboardUsersController::class, 'notificationsSettings'])->name('dashboard-notifications-settings');
            Route::post('settings/notifications', [DashboardUsersController::class, 'updateNotificationsSettings'])->name('update-notifications-settings');

            Route::get('settings/payments', [DashboardUsersController::class, 'paymentsSettings'])->name('dashboard-payments-settings');
            Route::get('settings/subscriptions', [DashboardUsersController::class, 'subscriptionsSettings'])->name('dashboard-subscriptions-settings');


            Route::get('card', [DashboardUsersController::class, 'card'])->name('dashboard-agency-card');
            Route::post('card', [DashboardUsersController::class, 'updateCard'])->name('update-card');

            Route::prefix('users')->group(function () {
                Route::get('', [DashboardUsersController::class, 'index'])->name('dashboard-users');
                Route::patch('{user}', [DashboardUsersController::class, 'update'])->name('update-user');
            });

            Route::prefix('candidates')->group(function () {
                Route::get('', [DashboardCandidatesController::class, 'indexActive'])->name('dashboard-candidates');
                Route::get('inactive', [DashboardCandidatesController::class, 'indexInactive'])->name('dashboard-candidates-inactive');
                Route::post('', [DashboardCandidatesController::class, 'store'])->name('store-candidate')->middleware(['can:create candidate']);
                Route::put('{candidate}', [DashboardCandidatesController::class, 'update'])->name('update-candidate')->middleware(['can:update candidate']);
                Route::patch('{candidate}/deactivate', [DashboardCandidatesController::class, 'deactivate'])->name('deactivate-candidate')->middleware(['can:deactivate candidate']);
                Route::patch('{candidate}/renew', [DashboardCandidatesController::class, 'renew'])->name('renew-candidate')->middleware(['can:renew candidate']);
                Route::delete('{candidate}', [DashboardCandidatesController::class, 'delete'])->name('delete-candidate')->middleware(['can:delete candidate']);
            });

            Route::get('attached-ads', [DashboardCandidatesController::class, 'attachedAds'])->name('dashboard-candidate-attached-ads');

            Route::prefix('ads')->group(function () {
                Route::get('', [DashboardAdsController::class, 'indexActive'])->name('dashboard-ads');
                Route::get('inactive', [DashboardAdsController::class, 'indexInactive'])->name('dashboard-ads-inactive');
                Route::post('', [DashboardAdsController::class, 'store'])->name('store-ad')->middleware(['can:create ad']);
                Route::put('{ad}', [DashboardAdsController::class, 'update'])->name('update-ad')->middleware(['can:update ad']);
                Route::delete('{ad}', [DashboardAdsController::class, 'delete'])->name('delete-ad')->middleware(['can:delete ad']);
                Route::patch('{ad}/candidates/{candidate}/proceed', [DashboardAdsController::class, 'proceedCandidate'])->name('proceed-ad-candidate')->middleware(['can:proceed ad candidate']);
                Route::patch('{ad}/candidates/{candidate}/reject', [DashboardAdsController::class, 'rejectCandidate'])->name('reject-ad-candidate')->middleware(['can:reject ad candidate']);
                Route::patch('{ad}/renew', [DashboardAdsController::class, 'renew'])->name('renew-ad')->middleware(['can:renew ad']);
                Route::patch('{ad}/deactivate', [DashboardAdsController::class, 'deactivate'])->name('deactivate-ad')->middleware(['can:deactivate ad']);
            });

            Route::prefix('admin')->group(function () {
                Route::get('ads', [DashboardAdminAdsController::class, 'indexModeration'])->name('dashboard-admin-ads')->middleware(['can:view admin ads']);
                Route::get('ads/active', [DashboardAdminAdsController::class, 'indexActive'])->name('dashboard-admin-ads-active')->middleware(['can:view admin ads']);
                Route::get('ads/inactive', [DashboardAdminAdsController::class, 'indexInactive'])->name('dashboard-admin-ads-inactive')->middleware(['can:view admin ads']);
                Route::get('candidates', [DashboardAdminCandidatesController::class, 'indexModeration'])->name('dashboard-admin-candidates')->middleware(['can:view admin candidates']);
                Route::get('candidates/active', [DashboardAdminCandidatesController::class, 'indexActive'])->name('dashboard-admin-candidates-active')->middleware(['can:view admin candidates']);
                Route::get('candidates/inactive', [DashboardAdminCandidatesController::class, 'indexInactive'])->name('dashboard-admin-candidates-inactive')->middleware(['can:view admin candidates']);
                Route::get('conversations', [DashboardAdminConversationsController::class, 'index'])->name('dashboard-admin-conversations')->middleware(['can:view admin conversations']);
                Route::get('conversations/{conversation}', [DashboardAdminConversationsController::class, 'fetch'])->name('dashboard-admin-conversation')->middleware(['can:view admin conversations']);

                Route::patch('ads/{ad}/accept', [DashboardAdminAdsController::class, 'acceptAd'])->name('admin-accept-ad')->middleware(['can:admin accept ad']);
                Route::patch('ads/{ad}/discard', [DashboardAdminAdsController::class, 'discardAd'])->name('admin-discard-ad')->middleware(['can:admin discard ad']);
                Route::patch('candidates/{candidate}/accept', [DashboardAdminCandidatesController::class, 'acceptCandidate'])->name('admin-accept-candidate')->middleware(['can:admin accept candidate']);
                Route::patch('candidates/{candidate}/discard', [DashboardAdminCandidatesController::class, 'discardCandidate'])->name('admin-discard-candidate')->middleware(['can:admin discard candidate']);
            });

            Route::get('conversations/{conversation}', [ConversationsController::class, 'fetch'])->name('dashboard-conversation')->middleware(['can:view conversations', 'can:view,conversation']);
            Route::get('conversations/{conversation}/buy-premium', [ConversationsController::class, 'fetchDemo'])->name('dashboard-conversation-demo')->middleware(['can:view conversations']);
            Route::post('conversations/{conversation}/message', [ConversationsController::class, 'sendMessage'])->name('dashboard-conversation-send-message')->middleware(['can:view conversations']);
            Route::get('conversations', [ConversationsController::class, 'index'])->name('dashboard-messages')->middleware(['can:view conversations']);
            Route::patch('conversations/{conversation}/starter-read', [ConversationsController::class, 'setStarterUserRead'])->name('conversation-set-starter-read')->middleware(['can:view conversations']);
            Route::patch('conversations/{conversation}/other-read', [ConversationsController::class, 'setOtherUserRead'])->name('conversation-set-other-read')->middleware(['can:view conversations']);
            Route::patch('conversations/{conversation}/admin-read', [ConversationsController::class, 'setAdminRead'])->name('conversation-set-admin-read')->middleware(['can:set admin conversation read']);
            Route::patch('conversations/{conversation}/hide', [ConversationsController::class, 'hide'])->name('conversation-hide')->middleware(['can:view,conversation']);
            Route::get('favorite-candidates', [DashboardCandidatesController::class, 'indexFavorites'])->name('dashboard-favorite-candidates')->middleware(['can:favorite candidate']);
            Route::get('favorite-ads', [DashboardAdsController::class, 'indexFavorites'])->name('dashboard-favorite-ads')->middleware(['can:favorite ad']);
            Route::get('read-ads-candidates', [DashboardAdsController::class, 'makeReadAdsCandidates'])->name('dashboard-read-ads-candidates');

            Route::get('favorite-candidate/{candidate}', [DashboardCandidatesController::class, 'favorite'])->name('favorite-candidate')->middleware(['can:favorite candidate']);
            Route::get('unfavorite-candidate/{userFavoriteCandidate}', [DashboardCandidatesController::class, 'unfavorite'])->name('unfavorite-candidate')->middleware(['can:unfavorite candidate']);
            Route::get('favorite-ad/{ad}', [DashboardAdsController::class, 'favorite'])->name('favorite-ad')->middleware(['can:favorite ad']);
            Route::get('unfavorite-ad/{userFavoriteAd}', [DashboardAdsController::class, 'unfavorite'])->name('unfavorite-ad')->middleware(['can:unfavorite ad']);

            Route::post('get-product-payment-ten', [PaymentsController::class, 'getProductPaymentTenCandidates'])->name('get-product-payment-ten-candidates');
            Route::post('get-product-payment-twenty', [PaymentsController::class, 'getProductPaymentTwentyCandidates'])->name('get-product-payment-twenty-candidates');
            Route::post('get-product-payment-fifty', [PaymentsController::class, 'getProductPaymentFiftyCandidates'])->name('get-product-payment-fifty-candidates');
            Route::post('get-subscription-payment', [PaymentsController::class, 'getSubscriptionPayment'])->name('get-subscription-payment');
            Route::get('cancel-subscription', [PaymentsController::class, 'cancelSubscription'])->name('cancel-subscription');
            Route::get('payments/{payment}', [PaymentsController::class, 'downloadInvoice'])->name('download-invoice');

        });
    });
});

Route::stripeWebhooks('stripe-webhook');

Route::get('tags/{tag}', [TagsController::class, 'search'])->name('find-tags');
