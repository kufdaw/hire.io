<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jan Kowalski',
            'password' => '$2y$12$qDvpwq4Kz2ZYc3b5..waVeLaFN6Lm1TVhKWgFIB6StkONoNOw7EoS',
            'email' => 'ktos@taki.pl',
        ]);
    }
}
