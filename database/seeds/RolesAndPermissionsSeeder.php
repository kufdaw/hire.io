<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'propose ad candidate']); //                         a
        Permission::create(['name' => 'remove ad candidate']); //                         a
        Permission::create(['name' => 'create conversation']); //                         c
        Permission::create(['name' => 'manage site settings']); //                         m
        Permission::create(['name' => 'create job type']); //                         m
        Permission::create(['name' => 'delete job type']); //                         m
        Permission::create(['name' => 'create language']); //                         m
        Permission::create(['name' => 'delete language']); //                         m
        Permission::create(['name' => 'manage admin payments']); //                         m
        Permission::create(['name' => 'create candidate']); //                         a
        Permission::create(['name' => 'update candidate']); //                         m, a
        Permission::create(['name' => 'deactivate candidate']); //                         m,a
        Permission::create(['name' => 'renew candidate']);  //                         m,a
        Permission::create(['name' => 'delete candidate']); //                         m,a
        Permission::create(['name' => 'create ad']); //                          c
        Permission::create(['name' => 'update ad']); //                         mc
        Permission::create(['name' => 'delete ad']); //                         mc
        Permission::create(['name' => 'proceed ad candidate']); //                         m, c
        Permission::create(['name' => 'reject ad candidate']); //                         m, c
        Permission::create(['name' => 'renew ad']); //                         m, c
        Permission::create(['name' => 'deactivate ad']); //                         m, c
        Permission::create(['name' => 'view admin ads']); //                         m
        Permission::create(['name' => 'view admin candidates']); //                         m
        Permission::create(['name' => 'view admin conversations']); //                         m
        Permission::create(['name' => 'admin accept ad']); //                         m
        Permission::create(['name' => 'admin discard ad']); //                         m
        Permission::create(['name' => 'admin accept candidate']); //                         m
        Permission::create(['name' => 'admin discard candidate']); //                         m
        Permission::create(['name' => 'view conversations']);                         //  c, a
        Permission::create(['name' => 'set admin conversation read']); //                         m
        Permission::create(['name' => 'favorite candidate']);                         // c
        Permission::create(['name' => 'unfavorite candidate']);                         // c
        Permission::create(['name' => 'favorite ad']); //                         a
        Permission::create(['name' => 'unfavorite ad']); //                         a

        Role::create(['name' => 'company'])->givePermissionTo([
            'create conversation',
            'create ad',
            'update ad',
            'delete ad',
            'deactivate ad',
            'renew ad',
            'proceed ad candidate',
            'reject ad candidate',
            'view conversations',
            'favorite candidate',
            'unfavorite candidate'
        ]);

        Role::create(['name' => 'agency'])->givePermissionTo([
            'propose ad candidate',
            'remove ad candidate',
            'create candidate',
            'update candidate',
            'deactivate candidate',
            'renew candidate',
            'delete candidate',
            'view conversations',
            'favorite ad',
            'unfavorite ad'
        ]);

        Role::create(['name' => 'moderator'])->givePermissionTo([
            'manage site settings',
            'create job type',
            'delete job type',
            'create language',
            'delete language',
            'manage admin payments',
            'update candidate',
            'deactivate candidate',
            'renew candidate',
            'delete candidate',
            'update ad',
            'delete ad',
            'proceed ad candidate',
            'reject ad candidate',
            'renew ad',
            'deactivate ad',
            'view admin ads',
            'view admin candidates',
            'view admin conversations',
            'admin accept ad',
            'admin discard ad',
            'admin accept candidate',
            'admin discard candidate',
            'set admin conversation read'
        ]);

        Role::create(['name' => 'super-admin'])->givePermissionTo(Permission::all());
    }
}
