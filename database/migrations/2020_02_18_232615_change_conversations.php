<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeConversations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conversations', function (Blueprint $table) {
            $table->dropColumn(['conversationable_id', 'conversationable_type']);
            $table->unsignedBigInteger('ad_id')->nullable();
            $table->unsignedBigInteger('candidate_id')->nullable();
            $table->unsignedBigInteger('starter_user_id');

            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
            $table->foreign('candidate_id')->references('id')->on('candidates')->onDelete('cascade');
            $table->foreign('starter_user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conversations', function (Blueprint $table) {
            $table->unsignedBigInteger('conversationable_id');
            $table->unsignedBigInteger('conversationable_type');

            $table->dropForeign(['ad_id']);
            $table->dropForeign(['candidate_id']);
            $table->dropForeign(['starter_user_id']);

            $table->dropColumn(['ad_id', 'candidate_id', 'starter_user_id']);
        });
    }
}
