<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TomtomCountryIdAndChangeRegionIdToCandidates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->string('tomtom_country_id')->nullable();
            $table->renameColumn('algolia_region_id', 'tomtom_region_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->dropColumn(['tomtom_country_id']);
            $table->renameColumn('tomtom_region_id', 'algolia_region_id');
        });
    }
}
