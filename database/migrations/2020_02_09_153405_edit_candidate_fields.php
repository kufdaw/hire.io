<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditCandidateFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->string('current_workplace')->nullable();
            $table->string('current_position')->nullable();
            $table->string('foreign_languages')->nullable();
            $table->string('skills');

            $table->dropColumn('salary_min', 'salary_max');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->dropColumn('current_workplace', 'current_position', 'foreign_languages', 'skills');

            $table->integer('salary_min')->nullable();
            $table->integer('salary_max')->nullable();
        });
    }
}
