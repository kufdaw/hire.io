<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewJobOffersEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $ads;

    /**
     * Create a new message instance.
     *
     * @param $ads
     */
    public function __construct($ads)
    {
        $this->ads = $ads;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.new-job-offers', [
            'ads' => $this->ads
        ])->subject('Nowe zlecenia w serwisie');
    }
}
