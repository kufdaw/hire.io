<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $registrationLink;

    /**
     * Create a new message instance.
     *
     * @param $registrationLink
     */
    public function __construct($registrationLink)
    {
        $this->registrationLink = $registrationLink;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.registration', [
            'registrationLink' => $this->registrationLink,
        ])->subject('Aktywacja konta');
    }
}
