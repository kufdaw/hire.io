<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CandidateAskedEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $candidateName;
    protected $link;

    /**
     * Create a new message instance.
     *
     * @param $candidateName
     * @param $link
     */
    public function __construct($candidateName, $link)
    {
        $this->candidateName = $candidateName;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.candidate-asked', [
            'candidateName' => $this->candidateName,
            'link' => $this->link
        ])->subject('Masz pytanie o kandydata');
    }
}
