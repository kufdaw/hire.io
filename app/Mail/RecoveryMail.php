<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecoveryMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $recoveryLink;

    /**
     * Create a new message instance.
     *
     * @param $recoveryLink
     */
    public function __construct($recoveryLink)
    {
        $this->recoveryLink = $recoveryLink;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.recovery', [
            'recoveryLink' => $this->recoveryLink,
        ])->subject('Zmiana hasła');
    }
}
