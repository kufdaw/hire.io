<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewCandidatesEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $candidates;

    /**
     * Create a new message instance.
     *
     * @param $candidates
     */
    public function __construct($candidates)
    {
        $this->candidates = $candidates;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.new-candidates', [
            'candidates' => $this->candidates
        ])->subject('Nowi kandydaci w serwisie');
    }
}
