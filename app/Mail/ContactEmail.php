<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $emailSender;
    protected $emailMessage;

    /**
     * Create a new message instance.
     *
     * @param $emailSender
     * @param $emailMessage
     */
    public function __construct($emailSender, $emailMessage)
    {
        $this->emailSender = $emailSender;
        $this->emailMessage = $emailMessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.contact', [
            'emailSender' => $this->emailSender,
            'emailMessage' => $this->emailMessage
        ]);
    }
}
