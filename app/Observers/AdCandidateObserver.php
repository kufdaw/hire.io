<?php

namespace App\Observers;


use App\Jobs\CandidateAskedEmailJob;
use App\Jobs\CandidateStatusChangedEmailJob;
use App\Mail\CandidateStatusChangedEmail;
use App\Models\AdCandidate;

class AdCandidateObserver
{
    /**
     * Handle the ad candidate "created" event.
     *
     * @param AdCandidate $adCandidate
     * @return void
     */
    public function created(AdCandidate $adCandidate)
    {
        $candidate = $adCandidate->candidate;
        $candidateOwner = $candidate->owner;
        $link = route('dashboard-candidates');

        dispatch(new CandidateAskedEmailJob($candidateOwner->email, $candidate->title, $link));
    }

    /**
     * Handle the ad candidate "updated" event.
     *
     * @param AdCandidate $adCandidate
     * @return void
     */
    public function updated(AdCandidate $adCandidate)
    {
    }

    /**
     * Handle the ad candidate "deleted" event.
     *
     * @param AdCandidate $adCandidate
     * @return void
     */
    public function deleted(AdCandidate $adCandidate)
    {
        //
    }

    /**
     * Handle the ad candidate "restored" event.
     *
     * @param AdCandidate $adCandidate
     * @return void
     */
    public function restored(AdCandidate $adCandidate)
    {
        //
    }

    /**
     * Handle the ad candidate "force deleted" event.
     *
     * @param AdCandidate $adCandidate
     * @return void
     */
    public function forceDeleted(AdCandidate $adCandidate)
    {
        //
    }
}
