<?php

namespace App\Observers;


use App\Jobs\CandidateAskedEmailJob;
use App\Models\Conversation;

class ConversationObserver
{
    /**
     * Handle the conversation "created" event.
     *
     * @param Conversation $conversation
     * @return void
     */
    public function created(Conversation $conversation)
    {
        $userReceiving = $conversation->candidate->owner;
        $link = route('dashboard-messages');

        dispatch(new CandidateAskedEmailJob($userReceiving->email, $conversation->candidate->title, $link));
    }

    /**
     * Handle the conversation "updated" event.
     *
     * @param Conversation $conversation
     * @return void
     */
    public function updated(Conversation $conversation)
    {
        //
    }

    /**
     * Handle the conversation "deleted" event.
     *
     * @param Conversation $conversation
     * @return void
     */
    public function deleted(Conversation $conversation)
    {
        //
    }

    /**
     * Handle the conversation "restored" event.
     *
     * @param Conversation $conversation
     * @return void
     */
    public function restored(Conversation $conversation)
    {
        //
    }

    /**
     * Handle the conversation "force deleted" event.
     *
     * @param Conversation $conversation
     * @return void
     */
    public function forceDeleted(Conversation $conversation)
    {
        //
    }
}
