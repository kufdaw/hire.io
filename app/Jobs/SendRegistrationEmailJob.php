<?php

namespace App\Jobs;

use App\Mail\RecoveryMail;
use App\Mail\RegistrationEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendRegistrationEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $registrationLink;
    protected $userEmail;

    /**
     * Create a new job instance.
     *
     * @param $userEmail
     * @param $registrationLink
     */
    public function __construct($userEmail, $registrationLink)
    {
        $this->userEmail = $userEmail;
        $this->registrationLink = $registrationLink;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->userEmail)->send(new RegistrationEmail($this->registrationLink));
    }
}
