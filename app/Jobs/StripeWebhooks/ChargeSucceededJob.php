<?php

namespace App\Jobs\StripeWebhooks;

use App\Models\Payment;
use App\Models\User;
use App\Services\Invoice\InvoiceService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Spatie\WebhookClient\Models\WebhookCall;

class ChargeSucceededJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $invoiceService = app(InvoiceService::class);
        $invoice = (object) [];
        $initialCharge = $this->webhookCall->payload['data']['object'];
        $user = User::where('stripe_id', $initialCharge['customer'])->first();

        if ($initialCharge['description'] === 'Subscription creation') {
            $meta = \Stripe\Charge::retrieve([
                "id" => $initialCharge['id'],
                "expand" => ["invoice.subscription"]
            ])['invoice']['lines']['data'][0]['metadata'];
        } else {
            $meta = $initialCharge['metadata'];
        }

        $data = [
            'email' => $user->email,
            'tax_id' => $meta['tax_id'] ?? null,
            'company_name' => $meta['company_name'] ?? null,
            'company_city' => $meta['company_city'] ?? null,
            'company_postal_code' => $meta['company_postal_code'] ?? null,
            'company_street' => $meta['company_street'] ?? null,
            'price' => $initialCharge['amount'] / 100,
            'product_name' => $meta['product_name'] ?? null
        ];

        try {
            if ($meta['create_invoice'] !== "false") {
                $invoice = $invoiceService->handleCreateInvoice($user, $data);
            }
        } catch (\Exception $e) {
        } finally {
            $payment = Payment::find($meta['payment_id']);
            $payment->update([
                'stripe_id' => $initialCharge['id'],
                'status' => 'succeeded',
                'fakturownia_invoice_id' => $invoice->id ?? null
            ]);
        }
    }
}
