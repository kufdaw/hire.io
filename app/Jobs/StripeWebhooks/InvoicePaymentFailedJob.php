<?php

namespace App\Jobs\StripeWebhooks;

use App\Models\Payment;
use App\Models\User;
use App\Services\Invoice\InvoiceService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Spatie\WebhookClient\Models\WebhookCall;

class InvoicePaymentFailedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $initialCharge = $this->webhookCall->payload['data']['object'];

        if ($initialCharge['object'] === 'invoice') {
            $meta = $initialCharge['lines']['data'][0]['metadata'];
        } else {
            $meta = \Stripe\Charge::retrieve([
                "id" => $initialCharge['id'],
                "expand" => ["invoice.subscription"]
            ])['invoice']['lines']['data'][0]['metadata'];
        }

        $payment = Payment::find($meta['payment_id']);
        $payment->update([
            'status' => 'failed',
        ]);
    }
}
