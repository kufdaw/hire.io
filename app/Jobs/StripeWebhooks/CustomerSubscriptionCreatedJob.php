<?php

namespace App\Jobs\StripeWebhooks;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Laravel\Cashier\Subscription;
use Spatie\WebhookClient\Models\WebhookCall;

class CustomerSubscriptionCreatedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $subscription = $this->webhookCall->payload['data']['object'];
        $user = User::where('stripe_id', $subscription['customer'])->first();

        Subscription::create([
            'user_id' => $user->id,
            'name' => 'Subscription',
            'stripe_id' => $subscription['id'],
            'stripe_status' => $subscription['status'],
            'stripe_plan' => $subscription['plan']['id'],
            'quantity' => $subscription['quantity'],
            'ends_at' => $subscription['current_period_end'],
        ]);
    }
}
