<?php

namespace App\Jobs;

use App\Mail\CandidateExpiredEmail;
use App\Mail\NewCandidatesEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class NewCandidatesEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userEmail;
    protected $candidates;

    /**
     * Create a new job instance.
     *
     * @param $userEmail
     * @param $candidates
     */
    public function __construct($userEmail, $candidates)
    {
        $this->userEmail = $userEmail;
        $this->candidates = $candidates;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->userEmail)->send(new NewCandidatesEmail($this->candidates));
    }
}
