<?php

namespace App\Jobs;

use App\Mail\CandidateAlmostExpiredEmail;
use App\Mail\CandidateExpiredEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class CandidateExpiredEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userEmail;
    protected $link;

    /**
     * Create a new job instance.
     *
     * @param $userEmail
     * @param $link
     */
    public function __construct($userEmail, $link)
    {
        $this->userEmail = $userEmail;
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->userEmail)->send(new CandidateExpiredEmail($this->link));
    }
}
