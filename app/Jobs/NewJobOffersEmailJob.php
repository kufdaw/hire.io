<?php

namespace App\Jobs;

use App\Mail\CandidateExpiredEmail;
use App\Mail\NewJobOffersEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class NewJobOffersEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userEmail;
    protected $ads;

    /**
     * Create a new job instance.
     *
     * @param $userEmail
     * @param $ads
     */
    public function __construct($userEmail, $ads)
    {
        $this->userEmail = $userEmail;
        $this->ads = $ads;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->userEmail)->send(new NewJobOffersEmail($this->ads));
    }
}
