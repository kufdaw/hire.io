<?php

namespace App\Jobs;

use App\Mail\RecoveryMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendRecoveryEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $recoveryLink;
    protected $userEmail;

    /**
     * Create a new job instance.
     *
     * @param $userEmail
     * @param $recoveryLink
     */
    public function __construct($userEmail, $recoveryLink)
    {
        $this->userEmail = $userEmail;
        $this->recoveryLink = $recoveryLink;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->userEmail)->send(new RecoveryMail($this->recoveryLink));
    }
}
