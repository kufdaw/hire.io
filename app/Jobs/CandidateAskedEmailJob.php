<?php

namespace App\Jobs;

use App\Mail\CandidateAlmostExpiredEmail;
use App\Mail\CandidateAskedEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class CandidateAskedEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userEmail;
    protected $candidateName;
    protected $link;

    /**
     * Create a new job instance.
     *
     * @param $userEmail
     * @param $candidateName
     * @param $link
     */
    public function __construct($userEmail, $candidateName, $link)
    {
        $this->userEmail = $userEmail;
        $this->candidateName = $candidateName;
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->userEmail)->send(new CandidateAskedEmail($this->candidateName, $this->link));
    }
}
