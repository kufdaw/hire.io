<?php

namespace App\Jobs;

use App\Mail\ContactEmail;
use App\Mail\NewCandidatesEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ContactEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $emailSender;
    public $emailMessage;

    /**
     * Create a new job instance.
     *
     * @param $emailSender
     * @param $emailMessage
     */
    public function __construct($emailSender, $emailMessage)
    {
        $this->emailSender = $emailSender;
        $this->emailMessage = $emailMessage;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to(config('app.contact_email'))->send(new ContactEmail($this->emailSender, $this->emailMessage));
    }
}
