<?php

namespace App\Jobs;

use App\Mail\CandidateExpiredEmail;
use App\Mail\CandidateStatusChangedEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class CandidateStatusChangedEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userEmail;
    protected $candidateName;
    protected $link;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userEmail, $candidateName, $link)
    {
        $this->userEmail = $userEmail;
        $this->candidateName = $candidateName;
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->userEmail)->send(new CandidateStatusChangedEmail($this->candidateName, $this->link));
    }
}
