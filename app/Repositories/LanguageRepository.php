<?php

namespace App\Repositories;

use App\Models\Language;
use Illuminate\Database\Eloquent\Collection;

class LanguageRepository
{
    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Language::all();
    }

    /**
     * @param array $data
     * @return Language
     */
    public function create(array $data): Language
    {
        return Language::create([
            'name' => $data['name']
        ]);
    }

    /**
     * @param Language $language
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Language $language)
    {
        return $language->delete();
    }
}
