<?php

namespace App\Repositories;

use App\Models\Payment;

class PaymentRepository
{
    public function getPaginated($skip, $take)
    {
        return Payment::orderBy('created_at', 'desc')->with('user')->skip($skip)->take($take)->get()->values();
    }
}
