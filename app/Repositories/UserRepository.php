<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return User::latest()->get();
    }

    /**
     * @param User $user
     * @param array $data
     * @return bool
     */
    public function update(User $user, array $data): bool
    {
        return $user->update([
//            'email' => $data['email'],
            'company_name' => $data['company_name'],
            'company_city' => $data['company_city'],
            'company_postal_code' => $data['company_postal_code'],
            'company_street' => $data['company_street'],
            'tax_id' => $data['tax_id'],
            'phone' => $data['phone'],
            'country_code' => $data['country_code'],
        ]);
    }

    /**
     * @param User $user
     * @param array $data
     * @return bool
     */
    public function updatePassword(User $user, array $data)
    {
        return $user->update([
            'password' => $data['password'] ? Hash::make($data['password']) : $user->getAuthPassword(),
        ]);
    }

    public function updateNotifications(User $user, array $data)
    {
        return $user->update([
            'notifications' => [
                'candidate_asked' => $data['notifications']['candidate_asked'],
                'candidate_expired' => $data['notifications']['candidate_expired'],
                'candidate_status_changed' => $data['notifications']['candidate_status_changed'],
                'new_candidates' => $data['notifications']['new_candidates'],
                'frequency' => $data['notifications']['frequency'] ?? null,
                'regions' => $data['notifications']['regions'] ?? null,
                'new_job_offers' => $data['notifications']['new_job_offers'],
                'new_message' => $data['notifications']['new_message'],
                'offer_expired' => $data['notifications']['offer_expired'],
                'job_types' => $data['notifications']['job_types']
            ]
        ]);
    }

    public function updateCard(User $user, array $data, $logoPath): bool
    {
        return $user->update([
            'address' => $data['address'] === 'null' ? null : $data['address'],
            'site' => $data['site'] === 'null' ? null : $data['site'],
            'public_phone' => $data['public_phone'] === 'null' ? null : $data['public_phone'],
            'description' => $data['description'] === 'null' ? null : $data['description'],
            'name' => $data['name'] === 'null' ? null : $data['name'],
            'logo' => $logoPath ?? $user->logo,
        ]);
    }

    /**
     * @param User $user
     * @param array $data
     * @return bool
     */
    public function patch(User $user, array $data): bool
    {
        return $user->update([
            'name' => $data['name'],
            'email' => $data['email']
        ]);
    }

    public function getPaginatedPremiumAgencies($skip, $take)
    {
        return User::role('agency')->whereHas('subscriptions', function ($query) {
            $query->where('stripe_status', 'active');
        })->orderBy('created_at', 'desc')->withCount('candidates')->skip($skip)->take($take)->get();
    }
}
