<?php

namespace App\Repositories;

use App\Models\Ad;
use App\Models\Candidate;
use App\Models\User;
use App\Models\UserFavoriteAd;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class AdRepository
{
    public function find(int $adId)
    {
        return Ad::withTrashed()->find($adId);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Ad::all()->sortByDesc('created_at');
    }

    public function active()
    {
        return Ad::active()->get();
    }

    public function inactive()
    {
        return Ad::inactive()->get();
    }

    public function moderation()
    {
        return Ad::moderation()->get();
    }

    public function besidesModeration()
    {
        return Ad::besidesModeration()->get();
    }

    /**
     * @param User $user
     * @return Builder[]|Collection|HasMany[]
     */
    public function getUserAdsWhenHasCandidate(User $user)
    {
        return $user->ads()->whereHas('candidates')->with('candidates')->get();
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function getGroupedCandidateAdsByUser(int $userId)
    {
        return Ad::whereHas('candidates', function ($query) use ($userId) {
            $query->where('owner_id', $userId);
        })->with('candidates')->get();
    }

    /**
     * @param User $user
     * @param array $data
     * @return Ad|Model
     */
    public function create(User $user, array $data)
    {
        return $user->ads()->create([
            'title'                 => $data['title'],
            'description'           => $data['description'],
            'salary_min'            => $data['salary_min'],
            'salary_max'            => $data['salary_max'],
            'experience_min'        => $data['experience_min'],
            'region'                => $data['region'],
            'tomtom_region_id'      => $data['tomtom_region_id'],
            'tomtom_country_id'     => $data['tomtom_country_id'],
            'job_type_id'           => $data['job_type_id'],
        ]);
    }

    /**
     * @param Ad $ad
     * @param array $data
     * @return bool
     */
    public function update(Ad $ad, array $data): bool
    {
        return $ad->update([
            'title'                 => $data['title'],
            'description'           => $data['description'],
            'salary_min'            => $data['salary_min'],
            'salary_max'            => $data['salary_max'],
            'experience_min'        => $data['experience_min'],
            'region'                => $data['region'],
            'tomtom_region_id'      => $data['tomtom_region_id'],
            'tomtom_country_id'     => $data['tomtom_country_id'],
            'status'                => $data['status'],
            'job_type_id'           => $data['job_type_id'],
        ]);
    }

    /**
     * @param Ad $ad
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Ad $ad): ?bool
    {
        return $ad->update([
            'deleted_at'        => Carbon::now(),
            'title'             => 'Deleted offer',
            'description'       => 'Deleted offer',
        ]);
    }

    /**
     * @param Ad $ad
     * @param int $candidateId
     */
    public function proposeCandidate(Ad $ad, int $candidateId)
    {
        return $ad->candidates()->attach($candidateId);
    }

    /**
     * @param int $adId
     * @param int $userId
     * @return mixed
     */
    public function createFavoriteAd(int $adId, int $userId)
    {
        return UserFavoriteAd::create([
            'ad_id' => $adId,
            'user_id' => $userId
        ]);
    }

    /**
     * @param UserFavoriteAd $userFavoriteAd
     * @return bool|null
     * @throws \Exception
     */
    public function deleteFavoriteAd(UserFavoriteAd $userFavoriteAd): ?bool
    {
        return $userFavoriteAd->delete();
    }

    public function removeCandidate(Ad $ad, int $candidateId)
    {
        return $ad->candidates()->detach($candidateId);
    }

    public function setAdCandidatesStatusRead(User $user)
    {
        $ads = $user->ads->pluck('id');

        return DB::table('ad_candidate')->whereIn('ad_id', $ads)->where('status', 'requested')->update([
            'status' => 'opened'
        ]);
    }

    public function proceedCandidate(int $adId, int $candidateId)
    {
        return DB::table('ad_candidate')->where('ad_id', $adId)->where('candidate_id', $candidateId)->update([
            'status' => 'proceeded'
        ]);
    }

    public function rejectCandidate(Ad $ad, Candidate $candidate)
    {
        return DB::table('ad_candidate')->where('ad_id', $ad->id)->where('candidate_id', $candidate->id)->update([
            'status' => 'rejected'
        ]);
    }

    /**
     * @param Ad $ad
     * @return bool
     */
    public function accept(Ad $ad): bool
    {
        return $ad->update([
            'status' => 'approved',
            'started_at' => Carbon::now()
        ]);
    }

    /**
     * @param Ad $ad
     * @param string $discardReason
     * @return bool
     */
    public function discard(Ad $ad, string $discardReason): bool
    {
        return $ad->update([
            'status' => 'discarded',
            'discard_reason' => $discardReason
        ]);
    }

    /**
     * @param Ad $ad
     * @return bool
     */
    public function deactivate(Ad $ad): bool
    {
        return $ad->update([
            'status' => 'deactivated'
        ]);
    }

    /**
     * @param Ad $ad
     * @return bool
     */
    public function renew(Ad $ad): bool
    {
        return $ad->update([
            'status' => 'unapproved'
        ]);
    }

    public function search(array $params)
    {
        return Ad::active()->search([
            'job_type_id' => $params['job_type_id'] ?? null,
            'language_id' => $params['language_id'] ?? null,
            'language_level' => $params['language_level'] ?? null,
            'is_country' => $params['is_country'] ?? null,
            'country_code' => $params['country_code'] ?? null,
            'title' => $params['title'] ?? null,
            'tags' => $params['tags'] ?? [],
            'tomtom_region_id' => $params['tomtom_region_id'] ?? ''
        ])->get();
    }
}
