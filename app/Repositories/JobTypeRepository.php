<?php

namespace App\Repositories;

use App\Models\JobType;
use Illuminate\Database\Eloquent\Collection;

class JobTypeRepository
{
    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return JobType::all();
    }

    /**
     * @param array $data
     * @return JobType
     */
    public function create(array $data): JobType
    {
        return JobType::create([
            'name' => $data['name']
        ]);
    }

    /**
     * @param JobType $jobType
     * @return bool|null
     * @throws \Exception
     */
    public function delete(JobType $jobType)
    {
        return $jobType->delete();
    }
}
