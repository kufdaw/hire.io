<?php

namespace App\Repositories;

use App\Models\Candidate;
use App\Models\User;
use App\Models\UserFavoriteCandidate;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CandidateRepository
{
    public function find(int $candidateId)
    {
        return Candidate::withTrashed()->find($candidateId);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Candidate::all();
    }

    public function active()
    {
        return Candidate::active()->get();
    }

    public function inactive()
    {
        return Candidate::inactive()->get();
    }

    public function moderation()
    {
        return Candidate::moderation()->get();
    }

    public function besidesModeration()
    {
        return Candidate::besidesModeration()->get();
    }

    /**
     * @param array $data
     * @param User $user
     * @return Candidate|Model
     */
    public function create(array $data, User $user)
    {
        return $user->candidates()->create([
            'name'                  => $data['name'],
            'title'                 => $data['title'],
            'private_description'   => $data['private_description'],
            'description'           => $data['description'],
            'contract_type'         => json_encode($data['contract_type']),
            'min_salary'            => $data['min_salary'],
            'max_salary'            => $data['max_salary'],
            'current_workplace'     => $data['current_workplace'],
            'current_position'      => $data['current_position'],
            'experience'            => $data['experience'],
            'region'                => $data['region'],
            'tomtom_region_id'      => $data['tomtom_region_id'],
            'tomtom_country_id'     => $data['tomtom_country_id'],
            'job_type_id'           => $data['job_type_id'],
        ]);
    }

    /**
     * @param array $data
     * @param Candidate $candidate
     * @return bool
     */
    public function update(array $data, Candidate $candidate): bool
    {
        return $candidate->update([
            'name'                  => $data['name'],
            'title'                 => $data['title'],
            'private_description'   => $data['private_description'],
            'description'           => $data['description'],
            'contract_type'         => $data['contract_type'],
            'min_salary'            => $data['min_salary'],
            'max_salary'            => $data['max_salary'],
            'current_workplace'     => $data['current_workplace'],
            'current_position'      => $data['current_position'],
            'experience'            => $data['experience'],
            'region'                => $data['region'],
            'tomtom_region_id'      => $data['tomtom_region_id'],
            'tomtom_country_id'     => $data['tomtom_country_id'],
            'status'                => $data['status'],
            'job_type_id'           => $data['job_type_id'],
        ]);
    }

    /**
     * @param Candidate $candidate
     * @return bool
     * @throws \Exception
     */
    public function delete(Candidate $candidate): bool
    {
        return $candidate->update([
            'deleted_at'            => Carbon::now(),
            'name'                  => 'Deleted candidate',
            'title'                 => 'Deleted candidate',
            'description'           => 'Deleted candidate',
            'current_workplace'     => 'Deleted candidate',
            'current_position'      => 'Deleted candidate',
            'private_description'   => 'Deleted candidate',
        ]);
    }

    /**
     * @param Candidate $candidate
     * @return bool
     */
    public function deactivate(Candidate $candidate): bool
    {
        return $candidate->update([
            'status' => 'deactivated'
        ]);
    }

    public function renew(Candidate $candidate)
    {
        return $candidate->update([
            'status' => 'unapproved'
        ]);
    }

    /**
     * @param int $candidateId
     * @param int $userId
     * @return mixed
     */
    public function createFavoriteCandidate(int $candidateId, int $userId)
    {
        return UserFavoriteCandidate::create([
            'candidate_id' => $candidateId,
            'user_id' => $userId
        ]);
    }

    /**
     * @param UserFavoriteCandidate $userFavoriteCandidate
     * @return bool|null
     * @throws \Exception
     */
    public function deleteFavoriteCandidate(UserFavoriteCandidate $userFavoriteCandidate): ?bool
    {
        return $userFavoriteCandidate->delete();
    }

    /**
     * @param Candidate $candidate
     * @param array $languages
     * @return array
     */
    public function syncLanguages(Candidate $candidate, array $languages): array
    {
        return $candidate->languages()->sync($languages);
    }

    /**
     * @param Candidate $candidate
     * @return bool
     */
    public function accept(Candidate $candidate): bool
    {
        return $candidate->update([
            'started_at' => Carbon::now(),
            'status' => 'approved'
        ]);
    }

    /**
     * @param Candidate $candidate
     * @param string $discardReason
     * @return bool
     */
    public function discard(Candidate $candidate, string $discardReason): bool
    {
        return $candidate->update([
            'status' => 'discarded',
            'discard_reason' => $discardReason
        ]);
    }

    public function search(array $params)
    {
        return Candidate::active()->search([
            'job_type_id' => $params['job_type_id'] ?? null,
            'language_id' => $params['language_id'] ?? null,
            'language_level' => $params['language_level'] ?? null,
            'is_country' => $params['is_country'] ?? null,
            'country_code' => $params['country_code'] ?? null,
            'title' => $params['title'] ?? null,
            'tags' => $params['tags'] ?? [],
            'tomtom_region_id' => $params['tomtom_region_id'] ?? ''
        ])->get();
    }
}
