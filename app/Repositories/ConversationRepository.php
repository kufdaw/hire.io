<?php

namespace App\Repositories;

use App\Models\Conversation;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ConversationRepository
{
    /**
     * @param array $data
     * @param int $starterUserId
     * @return mixed
     */
    public function create(array $data, int $starterUserId)
    {
        return Conversation::create([
            'ad_id' => $data['ad_id'] ?? null,
            'candidate_id' => $data['candidate_id'],
            'starter_user_id' => $starterUserId,
            'is_starter_read' => true
        ]);
    }

    /**
     * @param Conversation $conversation
     * @param string $text
     * @param int $starterUserId
     * @return Model
     */
    public function createMessage(Conversation $conversation, string $text, int $starterUserId)
    {
        return $conversation->messages()->create([
            'text' => $text,
            'user_id' => $starterUserId
        ]);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function allUsers(User $user)
    {
        return Conversation::where(function ($query) use ($user) {
                $query->where('starter_user_id', $user->id)->where('is_starter_hidden', false);
            })
            ->orWhereHas('ad', function ($query) use ($user) {
                $query->where('owner_id', $user->id);
            })->orWhereHas('candidate', function ($query) use ($user) {
                $query->where('owner_id', $user->id)->where('is_other_hidden', false);
            })->orderBy('id', 'desc')->with(['lastMessage', 'ad', 'ad.owner', 'starter', 'candidate', 'candidate.owner'])->get();
    }

    /**
     * @return Conversation[]|Collection
     */
    public function all()
    {
        return Conversation::all();
    }

    public function setStarterUserRead(Conversation $conversation)
    {
        return $conversation->update([
            'is_starter_read' => 1
        ]);
    }

    public function setOtherUserRead(Conversation $conversation)
    {
        return $conversation->update([
            'is_other_read' => 1
        ]);
    }

    public function setAdminRead(Conversation $conversation)
    {
        return $conversation->update([
            'is_admin_read' => 1
        ]);
    }

    public function setStarterUserUnread(Conversation $conversation)
    {
        return $conversation->update([
            'is_starter_read' => 0,
            'is_admin_read' => 0
        ]);
    }

    public function setOtherUserUnread(Conversation $conversation)
    {
        return $conversation->update([
            'is_other_read' => 0,
            'is_admin_read' => 0
        ]);
    }

    public function hideStarter(Conversation $conversation)
    {
        return $conversation->update([
            'is_starter_hidden' => true
        ]);
    }

    public function hideOther(Conversation $conversation)
    {
        return $conversation->update([
            'is_other_hidden' => true
        ]);
    }
}
