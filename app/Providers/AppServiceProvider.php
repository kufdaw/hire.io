<?php

namespace App\Providers;

use App\Models\Payment;
use App\Observers\PaymentObserver;
use App\Repositories\ConversationRepository;
use App\Services\Ad\AdFetcher;
use App\Services\Candidate\CandidateFetcher;
use App\Services\Conversation\ConversationFetcher;
use App\Services\Stripe\StripeService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Payment::observe(PaymentObserver::class);

        Inertia::share([
            // Synchronously
            'app' => [
                'name' => Config::get('app.name')
            ],
            // Lazily
            'auth' => function () {
                return [
                    'user' => Auth::user() ? [
                        'id' => Auth::user()->id,
                        'name' => Auth::user()->name,
                        'email' => Auth::user()->email,
                        'subscription' => Auth::user()->getSubscription(),
                        'description' => Auth::user()->description,
                        'country_code' => Auth::user()->country_code,
                        'role' => Auth::user()->roles[0]->name,
                        'my_ads_attachments' => DB::table('ad_candidate')->whereIn('ad_id', Auth::user()->ads->pluck('id'))->where('status', 'requested')->get()->count(),
                        'unread_messages_count' => app(ConversationFetcher::class)->unreadMessagesCount(Auth::user()),
                        'admin_unread_messages_count' => app(ConversationFetcher::class)->adminUnreadMessagesCount(Auth::user()),
                        'moderation_ads_count' => app(AdFetcher::class)->moderation()->count(),
                        'moderation_candidates_count' => app(CandidateFetcher::class)->moderation()->count()
                    ] : null,
                ];
            },
            'flash' => function () {
                return [
                    'success' => Session::get('success'),
                ];
            },
            'errors' => function () {
                return Session::get('errors')
                    ? Session::get('errors')->getBag('default')->getMessages()
                    : (object) [];
            },
        ]);
    }
}
