<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SearchCandidatesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'nullable|string',
            'language_id' => 'nullable|numeric',
            'language_level' => 'nullable|string',
            'tags' => 'nullable|array',
            'tomtom_region_id' => 'nullable|array',
            'job_type_id' => 'nullable|exists:job_types,id',
            'skip' => 'nullable|numeric',
            'is_country' => 'nullable',
            'country_code' => 'nullable'
        ];
    }
}
