<?php

namespace App\Http\Requests\DashboardCandidate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCandidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                          => 'nullable|string',
            'title'                         => 'required|string',
            'private_description'           => 'nullable|string',
            'experience'                    => 'required|numeric',
            'contract_type'                 => 'required|array',
            'min_salary'                    => 'required|string',
            'max_salary'                    => 'required|string',
            'description'                   => 'required|string',
            'current_workplace'             => 'nullable|string',
            'current_position'              => 'nullable|string',
            'languages'                     => 'nullable|array',
            'languages.*.language_id'       => 'exists:languages,id',
            'languages.*.language_level'    => [Rule::in(['A1', 'A2', 'B1', 'B2', 'C1', 'C2'])],
            'skills'                        => 'required|array',
            'region'                        => 'required|string',
            'tomtom_region_id'              => 'nullable|string',
            'tomtom_country_id'             => 'nullable|string',
            'job_type_id'                   => 'required|exists:job_types,id',
        ];
    }
}
