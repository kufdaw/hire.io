<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'nullable|string|max:255',
            'site' => 'nullable|string|max:255',
            'public_phone' => 'nullable|string|max:255',
            'description' => 'nullable|string',
            'name' => 'required|string|max:255',
            'logo' => 'nullable|image|max:2048|mimes:jpg,png,jpeg'
        ];
    }
}
