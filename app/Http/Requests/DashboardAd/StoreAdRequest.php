<?php

namespace App\Http\Requests\DashboardAd;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description'           => 'required|string',
            'salary_min'            => 'nullable|string',
            'salary_max'            => 'nullable|string',
            'region'                => 'required|string',
            'tomtom_region_id'      => 'nullable|string',
            'tomtom_country_id'      => 'nullable|string',
            'experience_min'        => 'required|numeric',
            'job_type_id'           => 'required|exists:job_types,id',
        ];
    }
}
