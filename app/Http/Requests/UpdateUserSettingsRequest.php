<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => ['nullable', 'string'],
            'phone' => ['nullable', 'string'],
            'tax_id' => ['nullable', 'string'],
            'company_name' => ['nullable', 'string'],
            'company_postal_code' => ['nullable', 'string'],
            'company_street' => ['nullable', 'string'],
            'country_code' => ['required', 'string', 'size:2'],
            'notifications' => ['required', 'array'],
//            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($this->user()->id)],
        ];
    }
}
