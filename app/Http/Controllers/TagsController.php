<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Tags\Tag;

class TagsController extends Controller
{
    /**
     * @param string $tag
     * @return mixed
     */
    public function search(string $tag)
    {
        return Tag::containing($tag)->get()->pluck('name');
    }
}
