<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\DashboardUpdateUserRequest;
use App\Http\Requests\UpdateUserCardRequest;
use App\Http\Requests\UpdateUserSettingsRequest;
use App\Http\Resources\DashboardUserResource;
use App\Http\Resources\UserCollection;
use App\Models\User;
use App\Services\Country\CountryFetcher;
use App\Services\Invoice\InvoiceService;
use App\Services\JobType\JobTypeFetcher;
use App\Services\Stripe\StripeService;
use App\Services\User\UserFetcher;
use App\Services\User\UserUpdater;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Inertia\Response;
use Spatie\Permission\Models\Role;

class DashboardUsersController extends Controller
{
    protected $userFetcher;
    protected $userUpdater;
    protected $countryFetcher;
    protected $stripeService;
    protected $invoiceService;
    protected $jobTypeFetcher;

    public function __construct(
        UserFetcher $userFetcher,
        UserUpdater $userUpdater,
        CountryFetcher $countryFetcher,
        StripeService $stripeService,
        InvoiceService $invoiceService,
        JobTypeFetcher $jobTypeFetcher
    ) {
        $this->userFetcher = $userFetcher;
        $this->userUpdater = $userUpdater;
        $this->countryFetcher = $countryFetcher;
        $this->stripeService = $stripeService;
        $this->invoiceService = $invoiceService;
        $this->jobTypeFetcher = $jobTypeFetcher;
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        return Inertia::render('Dashboard/Users', [
            'users' => new UserCollection($this->userFetcher->all())
        ]);
    }

    public function update(DashboardUpdateUserRequest $request, User $user)
    {
        $this->userUpdater->patch($user, $request->validated());
        return redirect()->back();
    }

    public function globalSettings()
    {
        return Inertia::render('Dashboard/Settings/GlobalSettings', [
            'countries' => $this->countryFetcher->all(),
            'payments' => auth()->user()->payments->sortByDesc('created_at')->values(),
            'candidatesCount' => auth()->user()->candidates()->moderationOrActive()->count(),
            'user' => auth()->user(),
            'candidatesLimit' => auth()->user()->countUserCandidatesLimit(),
            'candidatesAmount' => auth()->user()->candidates->count(),
            'jobTypes' => $this->jobTypeFetcher->all()
        ]);
    }

    public function notificationsSettings()
    {
        return Inertia::render('Dashboard/Settings/NotificationsSettings', [
            'user' => auth()->user(),
            'jobTypes' => $this->jobTypeFetcher->all()
        ]);
    }

    public function paymentsSettings()
    {
        return Inertia::render('Dashboard/Settings/PaymentsSettings', [
            'payments' => auth()->user()->payments->sortByDesc('created_at')->values(),
            'user' => auth()->user(),
        ]);
    }

    public function subscriptionsSettings()
    {
        return Inertia::render('Dashboard/Settings/SubscriptionsSettings', [
            'candidatesCount' => auth()->user()->candidates()->moderationOrActive()->count(),
            'user' => auth()->user(),
            'candidatesLimit' => auth()->user()->countUserCandidatesLimit(),
            'candidatesAmount' => auth()->user()->candidates->count(),
        ]);
    }

    /**
     * @param UpdateUserSettingsRequest $request
     * @return RedirectResponse
     */
    public function updateGlobalSettings(UpdateUserSettingsRequest $request)
    {
        $this->userUpdater->update($request->user(), $request->all());

        return redirect()->back();
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $this->userUpdater->updatePassword($request->user(), $request->validated());

        return redirect()->back();
    }

    public function updateNotificationsSettings(Request $request)
    {
        $this->userUpdater->updateNotifications($request->user(), $request->all());

        return redirect()->back();
    }

    public function updateCard(UpdateUserCardRequest $request)
    {
        $logoPath = null;

        if ($request->hasFile('logo')) {
            $oldLogoPath = $request->user()->logo;

            $logoPath = Storage::disk('public')->put('logo', $request->file('logo'));

            Storage::delete($oldLogoPath);
        }

        $this->userUpdater->updateCard($request->user(), $request->all(), $logoPath);

        return redirect()->back();
    }

    public function card()
    {
        return Inertia::render('Dashboard/AgencyCard', [
            'user' => new DashboardUserResource(auth()->user())
        ]);
    }
}

