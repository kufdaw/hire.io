<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserSettingsRequest;
use App\Http\Resources\UserCollection;
use App\Services\Payment\PaymentFetcher;
use App\Services\User\UserFetcher;
use App\Services\User\UserUpdater;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class DashboardPaymentsController extends Controller
{
    protected $paymentFetcher;

    public function __construct(
        PaymentFetcher $paymentFetcher
    ) {
        $this->paymentFetcher = $paymentFetcher;
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        $initialPayments = $this->paymentFetcher->getPaginated(0, 25);

        return Inertia::render('Dashboard/Payments', [
            'initialPayments' => $initialPayments
        ]);
    }

    public function getPaginated(Request $request)
    {
        return response()->json([
            'data' => $this->paymentFetcher->getPaginated($request->get('skip'), 15)
        ]);
    }
}
