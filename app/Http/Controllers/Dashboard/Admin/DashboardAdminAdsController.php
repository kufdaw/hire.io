<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Http\Requests\DiscardAdRequest;
use App\Http\Resources\DashboardAdCollection;
use App\Models\Ad;
use App\Services\Ad\AdFetcher;
use App\Services\Ad\AdUpdater;
use App\Services\JobType\JobTypeFetcher;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;

class DashboardAdminAdsController
{
    protected $adFetcher;
    protected $adUpdater;
    protected $jobTypeFetcher;

    /**
     * DashboardAdminAdsController constructor.
     * @param AdFetcher $adFetcher
     * @param AdUpdater $adUpdater
     * @param JobTypeFetcher $jobTypeFetcher
     */
    public function __construct(
        AdFetcher $adFetcher,
        AdUpdater $adUpdater,
        JobTypeFetcher $jobTypeFetcher
    ) {
        $this->adFetcher = $adFetcher;
        $this->adUpdater = $adUpdater;
        $this->jobTypeFetcher = $jobTypeFetcher;
    }

    /**
     * @return \Inertia\Response
     */
    public function indexActive(): \Inertia\Response
    {
        $activeAds = $this->adFetcher->active()->load('candidates')->sortByDesc('created_at');
        $inactiveAds = $this->adFetcher->besidesModeration();
        $moderationAds = $this->adFetcher->moderation();

        return Inertia::render('Dashboard/Admin/Ads/Ads', [
            'ads' => new DashboardAdCollection($activeAds),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'activeAdsAmount' => $activeAds->count(),
            'inactiveAdsAmount' => $inactiveAds->count(),
            'moderationAdsAmount' => $moderationAds->count(),
            'adsType' => 'active',
        ]);
    }

    /**
     * @return \Inertia\Response
     */
    public function indexInactive(): \Inertia\Response
    {
        $activeAds = $this->adFetcher->active();
        $inactiveAds = $this->adFetcher->besidesModeration()->load('candidates')->sortByDesc('created_at');
        $moderationAds = $this->adFetcher->moderation();

        return Inertia::render('Dashboard/Admin/Ads/Ads', [
            'ads' => new DashboardAdCollection($inactiveAds),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'activeAdsAmount' => $activeAds->count(),
            'inactiveAdsAmount' => $inactiveAds->count(),
            'moderationAdsAmount' => $moderationAds->count(),
            'adsType' => 'inactive',
        ]);
    }

    /**
     * @return \Inertia\Response
     */
    public function indexModeration(): \Inertia\Response
    {
        $activeAds = $this->adFetcher->active();
        $inactiveAds = $this->adFetcher->besidesModeration();
        $moderationAds = $this->adFetcher->moderation()->load('candidates')->sortByDesc('created_at');

        return Inertia::render('Dashboard/Admin/Ads/Ads', [
            'ads' => new DashboardAdCollection($moderationAds),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'activeAdsAmount' => $activeAds->count(),
            'inactiveAdsAmount' => $inactiveAds->count(),
            'moderationAdsAmount' => $moderationAds->count(),
            'adsType' => 'moderation',
        ]);
    }

    public function acceptAd(Ad $ad): RedirectResponse
    {
        $this->adUpdater->accept($ad);

        return redirect()->back();
    }

    public function discardAd(Ad $ad, DiscardAdRequest $request): RedirectResponse
    {
        $this->adUpdater->discard($ad, $request->get('discard_reason'));

        return redirect()->back();
    }
}
