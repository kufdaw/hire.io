<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Http\Requests\DiscardCandidateRequest;
use App\Http\Resources\DashboardAdCollection;
use App\Http\Resources\DashboardCandidateCollection;
use App\Models\Candidate;
use App\Services\Candidate\CandidateFetcher;
use App\Services\Candidate\CandidateUpdater;
use App\Services\JobType\JobTypeFetcher;
use App\Services\Language\LanguageFetcher;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;

class DashboardAdminCandidatesController
{
    protected $candidateFetcher;
    protected $candidateUpdater;
    protected $jobTypeFetcher;
    protected $languageFetcher;

    /**
     * DashboardAdminCandidatesController constructor.
     * @param CandidateFetcher $candidateFetcher
     * @param CandidateUpdater $candidateUpdater
     * @param JobTypeFetcher $jobTypeFetcher
     * @param LanguageFetcher $languageFetcher
     */
    public function __construct(
        CandidateFetcher $candidateFetcher,
        CandidateUpdater $candidateUpdater,
        JobTypeFetcher $jobTypeFetcher,
        LanguageFetcher $languageFetcher
    ) {
        $this->candidateFetcher = $candidateFetcher;
        $this->candidateUpdater = $candidateUpdater;
        $this->jobTypeFetcher = $jobTypeFetcher;
        $this->languageFetcher = $languageFetcher;
    }

    /**
     * @return \Inertia\Response
     */
    public function indexActive(): \Inertia\Response
    {
        $activeCandidates = $this->candidateFetcher->active()->load('jobType', 'ads', 'owner')->sortByDesc('created_at');
        $inactiveCandidates = $this->candidateFetcher->besidesModeration();
        $moderationCandidates = $this->candidateFetcher->moderation();

        return Inertia::render('Dashboard/Admin/Candidates/Candidates', [
            'candidates' => new DashboardCandidateCollection($activeCandidates),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'languages' => $this->languageFetcher->all(),
            'activeCandidatesAmount' => $activeCandidates->count(),
            'inactiveCandidatesAmount' => $inactiveCandidates->count(),
            'moderationCandidatesAmount' => $moderationCandidates->count(),
            'candidatesType' => 'active'
        ]);
    }

    /**
     * @return \Inertia\Response
     */
    public function indexInactive(): \Inertia\Response
    {
        $activeCandidates = $this->candidateFetcher->active();
        $inactiveCandidates = $this->candidateFetcher->besidesModeration()->load('jobType', 'ads', 'owner')->sortByDesc('created_at');
        $moderationCandidates = $this->candidateFetcher->moderation();

        return Inertia::render('Dashboard/Admin/Candidates/Candidates', [
            'candidates' => new DashboardCandidateCollection($inactiveCandidates),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'languages' => $this->languageFetcher->all(),
            'activeCandidatesAmount' => $activeCandidates->count(),
            'inactiveCandidatesAmount' => $inactiveCandidates->count(),
            'moderationCandidatesAmount' => $moderationCandidates->count(),
            'candidatesType' => 'inactive'
        ]);
    }

    public function indexModeration()
    {
        $activeCandidates = $this->candidateFetcher->active();
        $inactiveCandidates = $this->candidateFetcher->besidesModeration()->load('jobType', 'ads', 'owner')->sortByDesc('created_at');
        $moderationCandidates = $this->candidateFetcher->moderation()->load('jobType', 'ads', 'owner')->sortByDesc('updated_at');

        return Inertia::render('Dashboard/Admin/Candidates/Candidates', [
            'candidates' => new DashboardCandidateCollection($moderationCandidates),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'languages' => $this->languageFetcher->all(),
            'activeCandidatesAmount' => $activeCandidates->count(),
            'inactiveCandidatesAmount' => $inactiveCandidates->count(),
            'moderationCandidatesAmount' => $moderationCandidates->count(),
            'candidatesType' => 'moderation'
        ]);
    }

    public function acceptCandidate(Candidate $candidate): RedirectResponse
    {
        $this->candidateUpdater->accept($candidate);

        return redirect()->back();
    }

    public function discardCandidate(Candidate $candidate, DiscardCandidateRequest $request): RedirectResponse
    {
        $this->candidateUpdater->discard($candidate, $request->get('discard_reason'));

        return redirect()->back();
    }
}
