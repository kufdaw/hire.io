<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Http\Resources\DashboardAdCollection;
use App\Models\Conversation;
use App\Services\Conversation\ConversationFetcher;
use Illuminate\Http\Request;
use Inertia\Inertia;

class DashboardAdminConversationsController
{
    protected $conversationFetcher;

    public function __construct(
        ConversationFetcher $conversationFetcher
    ) {
        $this->conversationFetcher = $conversationFetcher;
    }

    public function index()
    {
        return Inertia::render('Dashboard/Admin/Messages/MessagesTab', [
            'conversations' => $this->conversationFetcher->all()->load('ad', 'candidate', 'starter', 'candidate.owner', 'ad.owner', 'lastMessage'),
            'conversation' => (object) []
        ]);
    }

    public function fetch(Conversation $conversation)
    {
        return Inertia::render('Dashboard/Admin/Messages/MessagesTab', [
            'conversations' => $this->conversationFetcher->all()->load('ad', 'candidate', 'starter', 'candidate.owner', 'ad.owner', 'lastMessage'),
            'conversation' => $conversation->load('messages', 'messages.user', 'candidate', 'ad', 'starter', 'candidate.owner', 'ad.owner')
        ]);
    }
}
