<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Services\Language\LanguageCreator;
use App\Services\Language\LanguageDeleter;
use Illuminate\Http\Request;

class LanguagesController extends Controller
{
    private $languageDeleter;
    private $languageCreator;

    /**
     * LanguagesController constructor.
     * @param LanguageDeleter $languageDeleter
     * @param LanguageCreator $languageCreator
     */
    public function __construct(
        LanguageDeleter $languageDeleter,
        LanguageCreator $languageCreator
    ) {
        $this->languageDeleter = $languageDeleter;
        $this->languageCreator = $languageCreator;
    }

    public function create(Request $request)
    {
        $this->languageCreator->create($request->all());

        return redirect()->back();
    }

    /**
     * @param Language $language
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Language $language)
    {
        $this->languageDeleter->delete($language);

        return redirect()->back();
    }
}
