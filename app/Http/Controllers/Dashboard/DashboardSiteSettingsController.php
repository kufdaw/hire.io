<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Services\JobType\JobTypeFetcher;
use App\Services\Language\LanguageFetcher;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class DashboardSiteSettingsController extends Controller
{
    private $languageFetcher;
    private $jobTypeFetcher;

    public function __construct(LanguageFetcher $languageFetcher, JobTypeFetcher $jobTypeFetcher)
    {
        $this->languageFetcher = $languageFetcher;
        $this->jobTypeFetcher = $jobTypeFetcher;
    }

    /**
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Dashboard/GlobalSettings/GlobalSettings', [
            'languages' => $this->languageFetcher->all(),
            'jobTypes' => $this->jobTypeFetcher->all()
        ]);
    }
}
