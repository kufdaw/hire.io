<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\JobType;
use App\Services\JobType\JobTypeCreator;
use App\Services\JobType\JobTypeDeleter;
use Illuminate\Http\Request;

class JobTypesController extends Controller
{
    private $jobTypeDeleter;
    private $jobTypeCreator;

    /**
     * JobTypesController constructor.
     * @param JobTypeDeleter $jobTypeDeleter
     * @param JobTypeCreator $jobTypeCreator
     */
    public function __construct(
        JobTypeDeleter $jobTypeDeleter,
        JobTypeCreator $jobTypeCreator
    ) {
        $this->jobTypeDeleter = $jobTypeDeleter;
        $this->jobTypeCreator = $jobTypeCreator;
    }

    /**
     * @param Request $request
     * @return JobType|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $this->jobTypeCreator->create($request->all());

        return redirect()->back();
    }

    /**
     * @param JobType $jobType
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(JobType $jobType)
    {
        $this->jobTypeDeleter->delete($jobType);

        return redirect()->back();
    }
}
