<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardCandidate\StoreCandidateRequest;
use App\Http\Requests\DashboardCandidate\UpdateCandidateRequest;
use App\Http\Resources\DashboardCandidateCollection;
use App\Http\Resources\FavoriteCandidateCollection;
use App\Models\UserFavoriteCandidate;
use App\Services\Ad\AdFetcher;
use App\Services\JobType\JobTypeFetcher;
use App\Services\Language\LanguageFetcher;
use App\Models\Candidate;
use App\Services\Candidate\CandidateCreator;
use App\Services\Candidate\CandidateDeleter;
use App\Services\Candidate\CandidateFetcher;
use App\Services\Candidate\CandidateUpdater;
use App\Services\Stripe\StripeService;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use Spatie\Tags\Tag;

class DashboardCandidatesController extends Controller
{
    protected $candidateFetcher;
    protected $candidateCreator;
    protected $candidateUpdater;
    protected $candidateDeleter;
    protected $jobTypeFetcher;
    protected $languageFetcher;
    protected $adFetcher;
    protected $stripeService;

    public function __construct(
        CandidateFetcher $candidateFetcher,
        CandidateCreator $candidateCreator,
        CandidateUpdater $candidateUpdater,
        CandidateDeleter $candidateDeleter,
        JobTypeFetcher $jobTypeFetcher,
        LanguageFetcher $languageFetcher,
        AdFetcher $adFetcher,
        StripeService $stripeService
    ) {
        $this->candidateFetcher = $candidateFetcher;
        $this->candidateCreator = $candidateCreator;
        $this->candidateUpdater = $candidateUpdater;
        $this->candidateDeleter = $candidateDeleter;
        $this->jobTypeFetcher = $jobTypeFetcher;
        $this->languageFetcher = $languageFetcher;
        $this->adFetcher = $adFetcher;
        $this->stripeService = $stripeService;
    }

    /**
     * @return Response
     */
    public function indexActive(): Response
    {
        return Inertia::render('Dashboard/DashboardCandidates/Candidates', [
            'candidates' => new DashboardCandidateCollection(auth()->user()->candidates()->active()->with('jobType', 'ads')->get()),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'languages' => $this->languageFetcher->all(),
            'activeCandidatesAmount' => auth()->user()->candidates()->active()->count(),
            'inactiveCandidatesAmount' => auth()->user()->candidates()->inactive()->count(),
            'candidateLimitReached' => auth()->user()->candidates()->moderationOrActive()->count() >= 10 + count($this->stripeService->getUserCandidatePayments()) * 10,
            'isAdmin' => false,
            'user' => auth()->user(),
            'areCandidatesActive' => true
        ]);
    }

    /**
     * @return Response
     */
    public function indexInactive(): Response
    {
        return Inertia::render('Dashboard/DashboardCandidates/Candidates', [
            'candidates' => new DashboardCandidateCollection(auth()->user()->candidates()->inactive()->with('jobType', 'ads')->get()),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'languages' => $this->languageFetcher->all(),
            'activeCandidatesAmount' => auth()->user()->candidates()->active()->count(),
            'inactiveCandidatesAmount' => auth()->user()->candidates()->inactive()->count(),
            'candidateLimitReached' => auth()->user()->candidates()->moderationOrActive()->count() >= 10 + count($this->stripeService->getUserCandidatePayments()) * 10,
            'isAdmin' => false,
            'user' => auth()->user(),
            'areCandidatesActive' => false
        ]);
    }

    public function renew(Candidate $candidate)
    {
        $this->candidateUpdater->renew($candidate);

        return redirect()->back();
    }

    /**
     * @param StoreCandidateRequest $request
     * @return RedirectResponse
     */
    public function store(StoreCandidateRequest $request): RedirectResponse
    {
        $this->candidateCreator->create($request->all(), $request->user());
        return redirect()->route('dashboard-candidates-inactive');
    }

    /**
     * @param Candidate $candidate
     * @param UpdateCandidateRequest $request
     * @return RedirectResponse
     */
    public function update(Candidate $candidate, UpdateCandidateRequest $request)
    {
        $this->candidateUpdater->update($request->all(), $candidate);
        return redirect()->route('dashboard-candidates-inactive');
    }

    /**
     * @param Candidate $candidate
     * @return RedirectResponse
     * @throws Exception
     */
    public function delete(Candidate $candidate): RedirectResponse
    {
        $this->candidateDeleter->delete($candidate);

        return redirect()->back();
    }

    /**
     * @param Candidate $candidate
     * @return RedirectResponse
     */
    public function deactivate(Candidate $candidate): RedirectResponse
    {
        $this->candidateUpdater->deactivate($candidate);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexFavorites(Request $request): Response
    {
        return Inertia::render('Dashboard/DashboardCandidates/FavoriteCandidates', [
            'favoriteCandidates' => new FavoriteCandidateCollection($request->user()->favoriteCandidates),
        ]);
    }

    /**
     * @param Request $request
     * @param Candidate $candidate
     * @return RedirectResponse
     */
    public function favorite(Request $request, Candidate $candidate)
    {
        $this->candidateCreator->createFavoriteCandidate($candidate->id, $request->user()->id);
        return redirect()->back();
    }

    /**
     * @param UserFavoriteCandidate $userFavoriteCandidate
     * @return RedirectResponse
     * @throws Exception
     */
    public function unfavorite(UserFavoriteCandidate $userFavoriteCandidate): RedirectResponse
    {
        $this->candidateDeleter->deleteFavoriteCandidate($userFavoriteCandidate);
        return redirect()->back();
    }

    public function attachedAds()
    {
        return Inertia::render('Dashboard/DashboardCandidates/Applications', [
            'applications' => $this->adFetcher->getGroupedCandidateAdsByUser(auth()->user()->id)
        ]);
    }

    /**
     * @param Collection $candidatesWithAds
     * @return array
     */
    protected function groupCandidatesByAds(Collection $candidatesWithAds): array // w sumie mozna byloby zrefaktoryzowac grupowaniem (recznie pogrupowac foreachem) i od razu dolaczaniem kandydatow
    {
        $applications = [];

        $groupedCandidates = $candidatesWithAds->groupBy(function ($item) {
            $ads = [];
            foreach ($item->ads as $ad) {
                $ads[] = $ad->id;
            }

            return $ads;
        });

        foreach ($groupedCandidates as $key => $group) {
            $candidates = $group;
            $application = $group[0]->ads->where('id', $key)->first();
            $application->setAttribute('candidates', $candidates->toArray());

            $applications[] = $application->toArray();
        }

        return $applications;
    }
}
