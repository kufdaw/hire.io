<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashboardAd\StoreAdRequest;
use App\Http\Requests\DashboardAd\UpdateAdRequest;
use App\Http\Resources\DashboardAdCollection;
use App\Models\Ad;
use App\Models\Candidate;
use App\Models\UserFavoriteAd;
use App\Services\Ad\AdCreator;
use App\Services\Ad\AdDeleter;
use App\Services\Ad\AdFetcher;
use App\Services\Ad\AdUpdater;
use App\Services\JobType\JobTypeFetcher;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class DashboardAdsController extends Controller
{
    private $adFetcher;
    private $adCreator;
    private $adUpdater;
    private $adDeleter;
    private $jobTypeFetcher;

    public function __construct(
        AdFetcher $adFetcher,
        AdCreator $adCreator,
        AdUpdater $adUpdater,
        AdDeleter $adDeleter,
        JobTypeFetcher $jobTypeFetcher
    ) {
        $this->adFetcher = $adFetcher;
        $this->adCreator = $adCreator;
        $this->adUpdater = $adUpdater;
        $this->adDeleter = $adDeleter;
        $this->jobTypeFetcher = $jobTypeFetcher;
    }

    /**
     * @return Response
     */
    public function indexActive(): Response
    {
        $activeAds = auth()->user()->ads()->active()->with('candidates')->get()->sortByDesc('created_at');
        $inactiveAds = auth()->user()->ads()->inactive();

        return Inertia::render('Dashboard/DashboardAds/Ads', [
            'ads' => new DashboardAdCollection($activeAds),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'activeAdsAmount' => $activeAds->count(),
            'inactiveAdsAmount' => $inactiveAds->count(),
            'areAdsActive' => true,
        ]);
    }

    /**
     * @return Response
     */
    public function indexInactive(): Response
    {
        $activeAds = auth()->user()->ads()->active();
        $inactiveAds = auth()->user()->ads()->inactive()->with('candidates')->get()->sortByDesc('created_at');

        return Inertia::render('Dashboard/DashboardAds/Ads', [
            'ads' => new DashboardAdCollection($inactiveAds),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'activeAdsAmount' => $activeAds->count(),
            'inactiveAdsAmount' => $inactiveAds->count(),
            'areAdsActive' => false,
        ]);
    }

    public function makeReadAdsCandidates()
    {
        $this->adUpdater->setAdCandidatesStatusRead(auth()->user());
    }

    /**
     * @param StoreAdRequest $request
     * @return RedirectResponse
     */
    public function store(StoreAdRequest $request)
    {
        $this->adCreator->create($request->user(), $request->all());
        return redirect()->route('dashboard-ads-inactive');
    }

    /**
     * @param Ad $ad
     * @param UpdateAdRequest $request
     * @return RedirectResponse
     */
    public function update(Ad $ad, UpdateAdRequest $request)
    {
        $this->adUpdater->update($ad, $request->all());
        return redirect()->route('dashboard-ads-inactive');
    }

    /**
     * @param Ad $ad
     * @return RedirectResponse
     * @throws \Exception
     */
    public function delete(Ad $ad)
    {
        $this->adDeleter->delete($ad);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexFavorites(Request $request): Response
    {
        return Inertia::render('Dashboard/DashboardAds/FavoriteAds', [
            'favoriteAds' => $request->user()->favoriteAds->load('ad'),
        ]);
    }

    /**
     * @param Request $request
     * @param Candidate $ad
     * @return RedirectResponse
     */
    public function favorite(Request $request, Candidate $ad)
    {
        $this->adCreator->createFavoriteAd($ad->id, $request->user()->id);
        return redirect()->back();
    }

    /**
     * @param UserFavoriteAd $userFavoriteAd
     * @return RedirectResponse
     * @throws \Exception
     */
    public function unfavorite(UserFavoriteAd $userFavoriteAd): RedirectResponse
    {
        $this->adDeleter->deleteFavoriteAd($userFavoriteAd);
        return redirect()->back();
    }

    /**
     * @param Ad $ad
     * @param Candidate $candidate
     * @return RedirectResponse
     */
    public function proceedCandidate(Ad $ad, Candidate $candidate): RedirectResponse
    {
        $this->adUpdater->proceedCandidate($ad, $candidate);

        return redirect()->back();
    }

    /**
     * @param Ad $ad
     * @param Candidate $candidate
     * @return RedirectResponse
     */
    public function rejectCandidate(Ad $ad, Candidate $candidate): RedirectResponse
    {
        $this->adUpdater->rejectCandidate($ad, $candidate);

        return redirect()->back();
    }

    public function deactivate(Ad $ad)
    {
        $this->adUpdater->deactivate($ad);

        return redirect()->back();
    }

    public function renew(Ad $ad)
    {
        $this->adUpdater->renew($ad);

        return redirect()->back();
    }
}
