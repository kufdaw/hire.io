<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\CandidateCollection;
use App\Models\User;
use App\Services\Country\CountryFetcher;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use Rinvex\Country\Country;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $countryFetcher;

    /**
     * Create a new controller instance.
     *
     * @param CountryFetcher $countryFetcher
     */
    public function __construct(CountryFetcher $countryFetcher)
    {
        $this->countryFetcher = $countryFetcher;
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param LoginRequest $request
     * @return Response|\Symfony\Component\HttpFoundation\Response
     * @throws ValidationException
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        $user = User::where('email', $request->get('email'))->first();

        if ($user) {
            if (!$user->registration_key) {
                if (Auth::attempt($credentials)) {
                    if ($user->hasRole('agency')) {
                        return redirect(route('dashboard-candidates'));
                    } else {
                        return redirect(route('candidates'));
                    }
                }
            }
        }


        return $this->sendFailedLoginResponse($request);
    }

    /**
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        Auth::logout();

        return redirect('');
    }

    /**
     * @return \Inertia\Response
     */
    public function indexJoin()
    {
        return Inertia::render('Join', [
            'countries' => $this->countryFetcher->all()
        ]);
    }
}
