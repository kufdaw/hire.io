<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Jobs\SendRecoveryEmailJob;
use App\Jobs\SendRegistrationEmailJob;
use App\Models\JobType;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param RegisterRequest $request
     */
    protected function create(RegisterRequest $request)
    {
        $role = Role::findByName($request->get('role'));
        $jobTypes = JobType::all()->pluck('id');

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'country_code' => $request->get('country_code'),
            'registration_key' => sha1(time()),
            'notifications' => '{"regions": [], "frequency": 30, "job_types": ' . $jobTypes . ', "new_message": true, "offer_expired": true, "new_candidates": true, "new_job_offers": true, "candidate_asked": true, "candidate_expired": true, "candidate_status_changed": true}'
        ]);

        $user->assignRole($role);

        $link = route('confirm-registration', $user->registration_key);

        dispatch(new SendRegistrationEmailJob($user->email, $link));

        return redirect()->back();
    }

    public function confirmRegistration(string $string)
    {
        $user = User::where('registration_key', $string)->first();

        if ($user) {
            $user->update([
                'registration_key' => null
            ]);
        }

        return Inertia::render('ConfirmRegistration', [
            'invalid' => is_null($user)
        ]);
    }
}
