<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RecoverAccountRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Jobs\SendRecoveryEmailJob;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function recover(RecoverAccountRequest $request)
    {
        $user = User::where('email', $request->get('recovery_email'))->first();

        if ($user) {
            $user->update([
                'recovery_key' => sha1(time()),
                'recovery_expiration' => Carbon::now()->addDays(2)->toDateTimeString()
            ]);
        }

        $link = route('reset-password', $user->recovery_key);

        dispatch(new SendRecoveryEmailJob($user->email, $link));

        return redirect()->back();
    }

    public function resetPassword(string $string)
    {
        $user = User::where('recovery_key', $string)->where('recovery_expiration', '>', Carbon::now()->toDateTimeString())->first();

        return Inertia::render('ResetPassword', [
            'email' => $user->email ?? null,
            'recoveryKey' => $user->recovery_key ?? null,
            'expired' => is_null($user)
        ]);
    }

    public function confirmResetPassword(ResetPasswordRequest $request)
    {
        $user = User::where('recovery_key', $request->get('recovery_key'))->where('recovery_expiration', '>', Carbon::now()->toDateTimeString())->first();

        if ($user) {
            $user->update([
                'password' => Hash::make($request->get('password')),
                'recovery_key' => null,
                'recovery_expiration' => null
            ]);
        }

        return redirect()->back();
    }
}
