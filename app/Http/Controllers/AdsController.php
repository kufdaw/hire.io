<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProposeCandidateRequest;
use App\Http\Requests\SearchAdsRequest;
use App\Http\Resources\AdCollection;
use App\Http\Resources\SingleAdResource;
use App\Models\Ad;
use App\Models\Candidate;
use App\Services\Ad\AdFetcher;
use App\Services\Ad\AdUpdater;
use App\Services\Candidate\CandidateFetcher;
use App\Services\JobType\JobTypeFetcher;
use App\Services\Language\LanguageFetcher;
use Illuminate\Http\Request;
use Inertia\Inertia;

class AdsController extends Controller
{
    private $adFetcher;
    private $adUpdater;
    private $jobTypeFetcher;
    private $candidateFetcher;
    private $languageFetcher;

    public function __construct(
        AdFetcher $adFetcher,
        AdUpdater $adUpdater,
        JobTypeFetcher $jobTypeFetcher,
        CandidateFetcher $candidateFetcher,
        LanguageFetcher $languageFetcher
    ) {
        $this->adFetcher = $adFetcher;
        $this->adUpdater = $adUpdater;
        $this->jobTypeFetcher = $jobTypeFetcher;
        $this->candidateFetcher = $candidateFetcher;
        $this->languageFetcher = $languageFetcher;
    }

    public function index()
    {
        $initialAds = $this->adFetcher->active()->load('jobType')->sortByDesc('started_at');

        return Inertia::render('Ads', [
            'ads' => new AdCollection($initialAds->take(10)),
            'initialAdsCount' => $initialAds->count(),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'languages' => $this->languageFetcher->all(),
            'favoriteAds' => auth()->user()->favoriteAds ?? []

        ]);
    }

    public function fetch(Ad $ad, Request $request)
    {
        $userCandidates = $request->user()->candidates ?? collect([]);
        $adCandidates = $ad->candidates;

        return Inertia::render('Ad', [
            'ad' => new SingleAdResource($ad),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'languages' => $this->languageFetcher->all(),
            'unproposedCandidates' => $this->candidateFetcher->getUnproposedCandidates($userCandidates, $adCandidates),
            'proposedCandidates' => $userCandidates->concat($adCandidates)->duplicates()->values()->all(),
            'isAdOwner' => auth()->user() ? $ad->owner_id === auth()->user()->id : false,
            'userConversations' => auth()->user()->conversations ?? []
        ]);
    }

    public function proposeCandidate(Ad $ad, ProposeCandidateRequest $request)
    {
        $this->adUpdater->proposeCandidate($ad, $request->candidate_id);

        return redirect()->back();
    }

    public function removeCandidate(Ad $ad, Candidate $candidate)
    {
        $this->adUpdater->removeCandidate($ad, $candidate->id);

        return redirect()->back();
    }

    public function search(SearchAdsRequest $request)
    {
        $ads = $this->adFetcher->search($request->all())->load('jobType')->sortByDesc('started_at');

        return response()->json([
            'data' => new AdCollection($ads->skip($request->get('skip'))->take(10)),
            'ads_count' => $ads->count()
        ]);
    }
}
