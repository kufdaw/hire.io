<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchCandidatesRequest;
use App\Http\Resources\CandidateCollection;
use App\Http\Resources\SingleCandidateResource;
use App\Models\Candidate;
use App\Services\Candidate\CandidateFetcher;
use App\Services\JobType\JobTypeFetcher;
use App\Services\Language\LanguageFetcher;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class CandidatesController extends Controller
{
    private $candidateFetcher;
    private $jobTypeFetcher;
    private $languageFetcher;

    public function __construct(
        CandidateFetcher $candidateFetcher,
        JobTypeFetcher $jobTypeFetcher,
        LanguageFetcher $languageFetcher
    ) {
        $this->candidateFetcher = $candidateFetcher;
        $this->jobTypeFetcher = $jobTypeFetcher;
        $this->languageFetcher = $languageFetcher;
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        $initialCandidates = $this->candidateFetcher->active()->load('jobType')->sortByDesc('started_at');

        return Inertia::render('Candidates', [
            'initialCandidates' => new CandidateCollection($initialCandidates->take(10)),
            'initialCandidatesCount' => $initialCandidates->count(),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'languages' => $this->languageFetcher->all(),
            'favoriteCandidates' => auth()->user()->favoriteCandidates ?? []
        ]);
    }

    /**
     * @param int $candidateId
     * @return Response
     */
    public function fetch(int $candidateId): Response
    {
        $candidate = $this->candidateFetcher->find($candidateId);

        return Inertia::render('Candidate', [
            'candidate' => new SingleCandidateResource($candidate),
            'candidates' => new CandidateCollection($this->candidateFetcher->active()->load('jobType')->take(10)),
            'jobTypes' => $this->jobTypeFetcher->all(),
            'languages' => $this->languageFetcher->all(),
            'agencyCandidateAds' => auth()->user() && auth()->user()->id === $candidate->owner_id ?  $candidate->ads : [],
            'isOwner' => auth()->user() && auth()->user()->id === $candidate->owner_id,
            'conversationExists' => auth()->user() ? auth()->user()->conversations()->where('candidate_id', $candidate->id)->get()->isNotEmpty() : false
        ]);
    }

    public function search(SearchCandidatesRequest $request)
    {
        $candidates = $this->candidateFetcher->search($request->all())->load('jobType')->sortByDesc('started_at');

        return response()->json([
            'data' => new CandidateCollection($candidates->skip($request->get('skip'))->take(10)),
            'candidates_count' => $candidates->count()
        ]);
    }
}
