<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendContactEmailRequest;
use App\Jobs\ContactEmailJob;
use Inertia\Inertia;

class ContactController extends Controller
{

    public function index()
    {
        return Inertia::render('Contact');
    }

    public function sendContactEmail(SendContactEmailRequest $request)
    {
        dispatch(new ContactEmailJob($request->get('email'), $request->get('message')));

        return redirect()->back();
    }
}
