<?php

namespace App\Http\Controllers;

use App\Http\Resources\AgencyCollection;
use App\Http\Resources\AgencyResource;
use App\Http\Resources\CandidateCollection;
use App\Http\Resources\SingleAgencyResource;
use App\Models\User;
use App\Services\User\UserFetcher;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\Permission\Exceptions\UnauthorizedException;

class AgenciesController
{
    protected $userFetcher;

    public function __construct(UserFetcher $userFetcher)
    {
        $this->userFetcher = $userFetcher;
    }

    public function index()
    {
        return Inertia::render('Agencies', [
            'initialAgencies' => new AgencyCollection($this->userFetcher->getPaginatedPremiumAgencies())
        ]);
    }

    public function getPaginated(Request $request)
    {
        return response()->json([
            'data' => new AgencyCollection($this->userFetcher->getPaginatedPremiumAgencies($request->get('skip'), 10))
        ]);
    }

    public function fetch(User $agency)
    {
        if ($agency->isPremiumActive() && $agency->hasRole('agency')) {
            return Inertia::render('Agency', [
                'agency' => new SingleAgencyResource($agency->load(['candidates' => function ($query) {
                    $query->take(10);
                }])),
                'favoriteCandidates' => auth()->user()->favoriteCandidates ?? []
            ]);
        }

        throw new UnauthorizedException(403);
    }

    public function getPaginatedAgencyCandidates(User $agency, Request $request)
    {
        return response()->json([
            'data' => new CandidateCollection($agency->candidates()->skip($request->get('skip'))->take(10)->get())
        ]);
    }
}
