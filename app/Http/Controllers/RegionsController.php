<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class RegionsController
{
    public function search(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $language = 'pl-PL'; // bcp47

        $response = $client->get('https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=' . env('HERE_API_TOKEN') . '&language=' . $language . '&query=' . $request->get('text'));

        $places = json_decode($response->getBody()->getContents());

        $parsedPlaces = [];

        foreach ($places->suggestions as $place) {
            if ($place->matchLevel === 'city') {
                $parsedPlaces[] = [
                    'is_country' => false,
                    'is_city' => true,
                    'id' => $place->locationId,
                    'name' => $place->address->city . ', ' . (isset($place->address->state) ? $place->address->state . ', ' :  '') . $place->address->country,
                    'country_code' => $place->countryCode,
                ];
            } else if ($place->matchLevel === 'country') {
                $parsedPlaces[] = [
                    'is_country' => true,
                    'is_city' => false,
                    'id' => $place->countryCode,
                    'name' => $place->label,
                    'country_code' => $place->countryCode,
                ];
            }
        }

        return response()->json([
            'data' => $parsedPlaces
        ]);
    }

    public function searchAround(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $parsedPlaces = [];

        if (strlen($request->get('location_id')) <= 4) {
            return response()->json([]);
        }

        $cityResp = $client->get('https://geocoder.ls.hereapi.com/6.2/geocode.json?apiKey=' . env('HERE_API_TOKEN') . '&locationid=' . $request->get('location_id'));
        $cityResponse = json_decode($cityResp->getBody()->getContents());

        for ($i = 1; $i < 20; $i++) {
            $response = $client->get('https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?gen=9&apiKey=' . env('HERE_API_TOKEN') . '&prox=' . $cityResponse->Response->View[0]->Result[0]->Location->DisplayPosition->Latitude . ',' . $cityResponse->Response->View[0]->Result[0]->Location->DisplayPosition->Longitude . ',' . $request->get('radius') * 1000 . '&mode=retrieveAreas&level=city&maxresults=100&pageInformation=' . $i);
            $places = json_decode($response->getBody()->getContents());

            foreach ($places->Response->View[0]->Result as $place) {
                $parsedPlaces[] = $place->Location->LocationId;
            }

            if (!isset($places->Response->MetaInfo->NextPageInformation)) {
                break;
            }
        }

        return response()->json($parsedPlaces);
    }
}
