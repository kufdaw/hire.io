<?php
namespace App\Http\Controllers;

use App\Http\Requests\CreateConversationRequest;
use App\Http\Requests\SendMessageRequest;
use App\Models\Conversation;
use App\Services\Conversation\ConversationCreator;
use App\Services\Conversation\ConversationFetcher;
use App\Services\Conversation\ConversationUpdater;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ConversationsController extends Controller
{
    protected $conversationCreator;
    protected $conversationFetcher;
    protected $conversationUpdater;

    public function __construct(
        ConversationCreator $conversationCreator,
        ConversationFetcher $conversationFetcher,
        ConversationUpdater $conversationUpdater
    ) {
        $this->conversationCreator = $conversationCreator;
        $this->conversationFetcher = $conversationFetcher;
        $this->conversationUpdater = $conversationUpdater;
    }

    public function index(Request $request)
    {
        $conversations = $this->conversationFetcher->allUsers($request->user());
        $latestConversation = (object) [];

        foreach (array_reverse($conversations) as $conversation) {
            if ($conversation->canUserView) {
                $latestConversation = $conversation->load('messages', 'messages.user');
            }
        }

        return Inertia::render('Dashboard/Messages/MessagesTab', [
            'conversations' => $conversations,
            'conversation' => $latestConversation,
            'user' => auth()->user()
        ]);
    }

    public function create(CreateConversationRequest $request)
    {
        $this->conversationCreator->create($request->all(), $request->user()->id);

        return redirect()->route('dashboard-messages');
    }

    public function fetch(Conversation $conversation, Request $request)
    {
        $conversations = $this->conversationFetcher->allUsers($request->user());

        return Inertia::render('Dashboard/Messages/MessagesTab', [
            'conversations' => $conversations,
            'conversation' => $conversation->load('messages', 'messages.user', 'candidate', 'ad', 'starter', 'candidate.owner', 'ad.owner'),
        ]);
    }

    public function hide(Conversation $conversation)
    {
        $this->conversationUpdater->hide($conversation, auth()->user()->id);

        return redirect()->route('dashboard-messages');
    }

    public function sendMessage(Conversation $conversation, SendMessageRequest $request)
    {
        $this->conversationCreator->createMessage($conversation, $request->get('text'), $request->user()->id);

        return redirect()->route('dashboard-conversation', $conversation->id);
    }

    public function setStarterUserRead(Conversation $conversation)
    {
        $this->conversationUpdater->setStarterUserRead($conversation);

        return redirect()->back();
    }

    public function setOtherUserRead(Conversation $conversation)
    {
        $this->conversationUpdater->setOtherUserRead($conversation);

        return redirect()->back();
    }

    public function setAdminRead(Conversation $conversation)
    {
        $this->conversationUpdater->setAdminRead($conversation);

        return redirect()->back();
    }
}
