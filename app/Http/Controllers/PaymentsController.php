<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Services\Invoice\InvoiceService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Stripe\Exception\CardException;

class PaymentsController extends Controller
{
    protected $invoiceService;

    public function __construct(InvoiceService $invoiceService)
    {
        $this->invoiceService = $invoiceService;
    }

    public function getProductPaymentTenCandidates(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $request->user()->update([
            'tax_id' => $request->get('tax_id'),
            'company_name' => $request->get('company_name'),
            'company_city' => $request->get('company_city'),
            'company_postal_code' => $request->get('company_postal_code'),
            'company_street' => $request->get('company_street'),
        ]);

        $price = \Stripe\Price::retrieve(env('STRIPE_10_PRODUCT_CANDIDATES_ID'));

        $payment = Payment::create([
            'user_id' => auth()->user()->id,
            'subtotal' => $price->unit_amount,
            'total' => $price->unit_amount,
            'status' => 'pending',
            'currency' => $price->currency,
            'type' => 'candidates',
            'amount' => 10
        ]);

        return \Stripe\PaymentIntent::create([
            'customer' => $request->user()->asStripeCustomer()->id,
            'amount' => $price->unit_amount,
            'currency' => $price->currency,
            'metadata' => [
                'integration_check' => 'accept_a_payment',
                'create_invoice' => (Boolean) $request->get('invoice_included'),
                'tax_id' => $request->get('tax_id'),
                'company_name' => $request->get('company_name'),
                'company_city' => $request->get('company_city'),
                'company_postal_code' => $request->get('company_postal_code'),
                'company_street' => $request->get('company_street'),
                'payment_id' => $payment->id,
                'product_name' => 'Extra candidates',
            ],
        ]);
    }

    public function getProductPaymentTwentyCandidates(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $request->user()->update([
            'tax_id' => $request->get('tax_id'),
            'company_name' => $request->get('company_name'),
            'company_city' => $request->get('company_city'),
            'company_postal_code' => $request->get('company_postal_code'),
            'company_street' => $request->get('company_street'),
        ]);

        $price = \Stripe\Price::retrieve(env('STRIPE_20_PRODUCT_CANDIDATES_ID'));

        $payment = Payment::create([
            'user_id' => auth()->user()->id,
            'subtotal' => $price->unit_amount,
            'total' => $price->unit_amount,
            'status' => 'pending',
            'currency' => $price->currency,
            'type' => 'candidates',
            'amount' => 20
        ]);

        return \Stripe\PaymentIntent::create([
            'customer' => $request->user()->asStripeCustomer()->id,
            'amount' => $price->unit_amount,
            'currency' => $price->currency,
            'metadata' => [
                'integration_check' => 'accept_a_payment',
                'create_invoice' => (Boolean) $request->get('invoice_included'),
                'tax_id' => $request->get('tax_id'),
                'company_name' => $request->get('company_name'),
                'company_city' => $request->get('company_city'),
                'company_postal_code' => $request->get('company_postal_code'),
                'company_street' => $request->get('company_street'),
                'payment_id' => $payment->id,
                'product_name' => 'Extra candidates'
            ],
        ]);
    }

    public function getProductPaymentFiftyCandidates(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $request->user()->update([
            'tax_id' => $request->get('tax_id'),
            'company_name' => $request->get('company_name'),
            'company_city' => $request->get('company_city'),
            'company_postal_code' => $request->get('company_postal_code'),
            'company_street' => $request->get('company_street'),
        ]);

        $price = \Stripe\Price::retrieve(env('STRIPE_50_PRODUCT_CANDIDATES_ID'));

        $payment = Payment::create([
            'user_id' => auth()->user()->id,
            'subtotal' => $price->unit_amount,
            'total' => $price->unit_amount,
            'status' => 'pending',
            'currency' => $price->currency,
            'type' => 'candidates',
            'amount' => 50
        ]);

        return \Stripe\PaymentIntent::create([
            'customer' => $request->user()->asStripeCustomer()->id,
            'amount' => $price->unit_amount,
            'currency' => $price->currency,
            'metadata' => [
                'integration_check' => 'accept_a_payment',
                'create_invoice' => (Boolean) $request->get('invoice_included'),
                'tax_id' => $request->get('tax_id'),
                'company_name' => $request->get('company_name'),
                'company_city' => $request->get('company_city'),
                'company_postal_code' => $request->get('company_postal_code'),
                'company_street' => $request->get('company_street'),
                'payment_id' => $payment->id,
                'product_name' => 'Extra candidates'
            ],
        ]);
    }

    public function getSubscriptionPayment(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $paymentMethodId = $request->get('payment_method_id');
        $customerId = $request->user()->asStripeCustomer()->id;

        $request->user()->update([
            'tax_id' => $request->get('tax_id'),
            'company_name' => $request->get('company_name'),
            'company_city' => $request->get('company_city'),
            'company_postal_code' => $request->get('company_postal_code'),
            'company_street' => $request->get('company_street'),
        ]);

        try {
            $paymentMethod = \Stripe\PaymentMethod::retrieve($paymentMethodId);
            $paymentMethod->attach([
                'customer' => $customerId
            ]);
        } catch (CardException $e) {
            if (isset($e->getJsonBody()->data)) {
                return response()->json($e->getJsonBody()->data);
            } else {
                return $e->getJsonBody();
            }
        }

        \Stripe\Customer::update($customerId, [
            'invoice_settings' => [
                'default_payment_method' => $paymentMethodId
            ]
        ]);

        $price = \Stripe\Price::retrieve(env('STRIPE_SUBSCRIPTION_ID'));

        $payment = Payment::create([
            'user_id' => auth()->user()->id,
            'stripe_id' => 1,
            'subtotal' => $price->unit_amount,
            'total' => $price->unit_amount,
            'status' => 'pending',
            'currency' => $price->currency,
            'type' => 'subscription',
        ]);

        $subscription = \Stripe\Subscription::create([
            'customer' => $customerId,
            'items' => [
                [
                    'plan' => env('STRIPE_SUBSCRIPTION_ID')
                ]
            ],
            'expand' => ['latest_invoice.payment_intent'],
            'metadata' => [
                'integration_check' => 'accept_a_payment',
                'create_invoice' => (Boolean) $request->get('invoice_included'),
                'tax_id' => $request->get('tax_id'),
                'company_name' => $request->get('company_name'),
                'company_city' => $request->get('company_city'),
                'company_postal_code' => $request->get('company_postal_code'),
                'company_street' => $request->get('company_street'),
                'payment_id' => $payment->id,
                'product_name' => 'Subscription'
            ],
        ]);

        return response()->json($subscription);
    }

    public function cancelSubscription()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $subscription = \Stripe\Subscription::retrieve(
            auth()->user()->subscriptions[0]->stripe_id
        );

        $subscription->delete();

        return redirect()->back();
    }

    public function downloadInvoice(Request $request, Payment $payment)
    {
        if ($payment->user_id === $request->user()->id || $request->user()->roles[0]->name === 'super-admin') {
            return response()->streamDownload(function () use ($payment) {
                echo file_get_contents('https://kufdaw.fakturownia.pl/invoices/' . $payment->fakturownia_invoice_id .'.pdf?api_token=' . env('FAKTUROWNIA_TOKEN'));
            }, Carbon::now() . '_invoice.pdf');
        }

        return redirect()->back();
    }
}
