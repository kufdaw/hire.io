<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CandidateLanguageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'language_id' => $this->pivot->language_id,
            'text' => $this->name . ' (' . $this->pivot->language_level . ')' ,
            'candidate_id' => $this->pivot->candidate_id,
            'language_level' => $this->pivot->language_level,
        ];
    }
}
