<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class SingleAgencyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'site'          => $this->site,
            'address'       => $this->address,
            'public_phone'  => $this->public_phone,
            'logo'          => $this->logo ? Storage::url($this->logo) : null,
            'description'   => $this->description,
            'name'          => $this->name,
            'candidates'    => new CandidateCollection($this->candidates)
        ];
    }
}
