<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class DashboardUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'created_at' => $this->created_at,
            'description' => $this->description,
            'logo' => $this->logo ? Storage::url($this->logo) : null,
            'country_code' => $this->country_code,
            'stripe_id' => $this->stripe_id,
            'card_brand' => $this->card_brand,
            'trial_ends_at' => $this->trial_ends_at,
            'tax_id' => $this->tax_id,
            'company_name' => $this->company_name,
            'company_postal_code' => $this->company_postal_code,
            'company_city' => $this->company_city,
            'company_street' => $this->company_street,
            'phone' => $this->phone,
            'public_phone' => $this->public_phobe,
            'site' => $this->site,
            'address' => $this->address,
            'subscriptions' => $this->subscriptions,
            'roles' => $this->roles,
        ];
    }
}
