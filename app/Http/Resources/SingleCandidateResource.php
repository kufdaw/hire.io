<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SingleCandidateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'name'                      => $this->name,
            'title'                     => $this->title,
            'description'               => $this->description,
            'min_salary'                => $this->min_salary,
            'max_salary'                => $this->max_salary,
            'status'                    => $this->status,
            'contract_type'             => json_decode($this->contract_type),
            'current_workplace'         => $this->current_workplace,
            'current_position'          => $this->current_position,
            'skills'                    => $this->tags->pluck('name'),
            'languages'                 => new CandidateLanguageCollection($this->languages),
            'experience'                => $this->experience,
            'job_type'                  => $this->jobType->name,
            'region'                    => $this->region,
            'tomtom_region_id'          => $this->tomtom_region_id,
            'tomtom_country_id'         => $this->tomtom_country_id,
            'owner'                     => $this->owner->name,
            'owner_id'                  => $this->owner->id,
            'is_agency_premium'         => $this->owner->isPremiumActive(),
            'job_type_id'               => $this->jobType->id,
            'deleted_at'                => $this->deleted_at,
            'created_at'                => $this->created_at,
            'started_at'                => $this->started_at,
        ];
    }
}
