<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DashboardCandidateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'job_type_id'               => $this->jobType->id,
            'candidates_selected_type'  => 'all',
            'name'                      => $this->name,
            'title'                     => $this->title,
            'description'               => $this->description,
            'private_description'       => $this->private_description,
            'current_workplace'         => $this->current_workplace,
            'min_salary'                => $this->min_salary,
            'max_salary'                => $this->max_salary,
            'discard_reason'            => $this->discard_reason,
            'status'                    => $this->status,
            'contract_type'             => json_decode($this->contract_type),
            'remaining_active_days'     => $this->remainingActiveDays(),
            'current_position'          => $this->current_position,
            'languages'                 => new CandidateLanguageCollection($this->languages),
            'skills'                    => $this->tags->pluck('name'),
            'experience'                => $this->experience,
            'job_type'                  => $this->jobType->name,
            'region'                    => $this->region,
            'tomtom_region_id'          => $this->tomtom_region_id,
            'tomtom_country_id'         => $this->tomtom_country_id,
            'owner'                     => $this->owner->name,
            'ads'                       => $this->ads,
            'deleted_at'                => $this->deleted_at,
            'created_at'                => $this->created_at,
            'started_at'                => $this->started_at
        ];
    }
}
