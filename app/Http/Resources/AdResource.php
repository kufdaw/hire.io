<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'title'                     => $this->title,
            'description'               => $this->description,
            'salary_min'                => $this->salary_min,
            'salary_max'                => $this->salary_max,
            'experience_min'            => $this->experience_min,
            'status'                    => $this->status,
            'job_type'                  => $this->jobType->name,
            'job_type_id'               => $this->jobType->id,
            'region'                    => $this->region,
            'tomtom_region_id'          => $this->tomtom_region_id,
            'tomtom_country_id'         => $this->tomtom_country_id,
            'owner'                     => $this->owner->name,
            'deleted_at'                => $this->deleted_at,
            'created_at'                => $this->created_at,
        ];
    }
}
