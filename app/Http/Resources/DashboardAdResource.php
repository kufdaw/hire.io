<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DashboardAdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'job_type_id'               => $this->jobType->id,
            'remaining_active_days'     => $this->remainingActiveDays(),
            'candidates_selected_type'  => 'all',
            'title'                     => $this->title,
            'description'               => $this->description,
            'status'                    => $this->status,
            'salary_min'                => $this->salary_min,
            'salary_max'                => $this->salary_max,
            'discard_reason'            => $this->discard_reason,
            'experience_min'            => $this->experience_min,
            'job_type'                  => $this->jobType->name,
            'region'                    => $this->region,
            'tomtom_region_id'          => $this->tomtom_region_id,
            'tomtom_country_id'         => $this->tomtom_country_id,
            'owner'                     => $this->owner->name,
            'candidates'                => $this->candidates,
            'deleted_at'                => $this->deleted_at,
            'created_at'                => $this->created_at
        ];
    }
}
