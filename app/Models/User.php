<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Subscription;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, Billable;

    protected $guarded = ['id'];
    protected $hidden = ['password', 'remember_token'];
    protected $casts = ['email_verified_at' => 'datetime'];

    protected const PREMIUM_DAYS = 30;

    /**
     * @return HasMany
     */
    public function candidates(): HasMany
    {
        return $this->hasMany(Candidate::class, 'owner_id');
    }

    /**
     * @return HasMany
     */
    public function ads(): HasMany
    {
        return $this->hasMany(Ad::class, 'owner_id');
    }

    /**
     * @return HasMany
     */
    public function favoriteCandidates(): HasMany
    {
        return $this->hasMany(UserFavoriteCandidate::class);
    }

    /**
     * @return HasMany
     */
    public function favoriteAds(): HasMany
    {
        return $this->hasMany(UserFavoriteAd::class);
    }

    /**
     * @return HasMany
     */
    public function conversations(): HasMany
    {
        return $this->hasMany(Conversation::class, 'starter_user_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class)->where('created_at', '>', Carbon::now()->subDays(365));
    }

    public function scopeIsPremium($query)
    {
        return $query->whereHas('payments', function ($query) {
            $query->where('status', 'paid')->where('created_at', '>=', Carbon::now()->subDays(self::PREMIUM_DAYS));
        })->count();
    }

    public function getSubscription()
    {
        $customer = $this->createOrGetStripeCustomer();
        $subscriptions = $this->subscriptions;

        foreach ($subscriptions as $subscription) {
            if ($subscription->stripe_status === 'active' || $subscription->stripe_status === 'canceled') {
                return [
                    'active' => $subscription->stripe_status === 'active' || $subscription->stripe_status === 'canceled' && Carbon::parse($subscription->ends_at) > Carbon::now(),
                    'cancel_at_period_end' => $subscription->stripe_status === 'canceled',
                    'period_end' => $subscription->ends_at
                ];
            }
        }

        return [
            'active' => false,
            'cancel_at_period_end' => null,
            'period_end' => null
        ];
    }

    public function isPremiumActive()
    {
        return $this->getSubscription()['active'];
    }

    public function countUserCandidatesLimit()
    {
        $payments = $this->payments()->where('status', 'succeeded')->where('type', 'candidates')->get();
        $amount = 0;

        foreach ($payments as $payment) {
            $amount += $payment->amount;
        }

        return $amount + 10;
    }
}
