<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected const ACTIVE_DAYS = 30;

    /**
     * @return BelongsTo
     */
    public function jobType(): BelongsTo
    {
        return $this->belongsTo(JobType::class);
    }

    /**
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function conversations()
    {
        return $this->hasMany(Conversation::class);
    }

    public function candidates()
    {
        return $this->belongsToMany(Candidate::class, 'ad_candidate')->withTimestamps()->withPivot('status');
    }

    public function scopeActive($query)
    {
        $query->where('status', 'approved')->where('started_at', '>=', Carbon::now()->subDays(self::ACTIVE_DAYS));
    }

    public function scopeInactive($query)
    {
        return $query->where('status', '!=', 'approved')->orWhere(function ($subQuery) {
            $subQuery->where('status', 'approved')->where('started_at', '<', Carbon::now()->subDays(self::ACTIVE_DAYS));
        });
    }

    public function scopeExpired($query)
    {
        return $query->where('started_at', '<', Carbon::now()->subDays(self::ACTIVE_DAYS));
    }

    public function scopeExpiredHoursAgo($query, int $hours)
    {
        return $query->where('started_at', '>', Carbon::now()->subHours((self::ACTIVE_DAYS * 24) + $hours))->where('started_at', '<', Carbon::now()->subHours((self::ACTIVE_DAYS * 24)));
    }

    public function scopeModeration($query)
    {
        return $query->where('status', 'unapproved');
    }

    public function scopeBesidesModeration($query)
    {
        return $this->scopeInactive($query->where('status', '!=', 'unapproved'));
    }

    public function remainingActiveDays()
    {
        if ($this->status === 'approved') {
            return self::ACTIVE_DAYS - (Carbon::now())->diffInDays($this->started_at);
        } else {
            return 0;
        }
    }

    public function scopeSearch($query, array $data)
    {
        return $query
            ->where('title', 'like',  '%' . $data['title'] . '%')
            ->where(function ($query) use ($data) {
                $query->when($data['tomtom_region_id'], function ($query) use ($data) {
                    $query->whereIn('tomtom_region_id', $data['tomtom_region_id']);
                })
                    ->when($data['is_country'], function ($query) use ($data) {
                        $query->orWhere('tomtom_country_id', $data['country_code']);
                    })
                    ->when($data['job_type_id'], function ($query) use ($data) {
                        $query->orWhere('job_type_id', $data['job_type_id']);
                    })
                    ->when($data['language_id'], function($query) use ($data) {
                        $query->orWhereHas('candidateLanguage', function ($query) use ($data) {
                            $query->where('language_id', $data['language_id'])->where('language_level', $data['language_level']);
                        });
                    })
                    ->when($data['tags'], function ($query) use ($data) {
                        $query->withAnyTags($data['tags']);
                    });
            });
    }
}
