<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CandidateLanguage extends Model
{
    protected $table = 'candidate_language';

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function candidate()
    {
        return $this->belongsTo(Candidate::class);
    }
}
