<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Conversation extends Model
{
    protected $guarded = ['id'];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function candidate()
    {
        return $this->belongsTo(Candidate::class, 'candidate_id')->withTrashed();
    }

    public function ad()
    {
        return $this->belongsTo(Ad::class, 'ad_id')->withTrashed();
    }

    public function starter()
    {
        return $this->belongsTo(User::class, 'starter_user_id');
    }

    public function scopeLastMessage()
    {
        return $this->messages()->latest();
    }

    public function hasStarted()
    {
        $conversation = $this;

        return $conversation->messages->contains(function ($message) use ($conversation) {
            return $message->user_id !== $conversation->starter_user_id;
        });
    }

    public function canUserConversate(int $userId)
    {
        if ($this->starter_user_id === $userId) {
            return true;
        }

        if ($this->ad) {
            if ($this->ad->owner_id === $userId) {
                return true;
            }
        }

        if ($this->candidate->owner_id === $userId) {
            return true;
        }

        return false;
    }
}
