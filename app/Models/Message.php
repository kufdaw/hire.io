<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function conversation() {
        return $this->belongsTo(Conversation::class);
    }

    public function scopeFromLastHours($query, int $hours)
    {
        return $query->where('created_at', '>', Carbon::now()->subHours($hours));
    }
}
