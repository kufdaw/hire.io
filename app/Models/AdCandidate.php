<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AdCandidate extends Model
{
    protected $table = 'ad_candidate';

    protected $guarded = ['id'];

    public function ad()
    {
        return $this->belongsTo(Ad::class);
    }

    public function candidate()
    {
        return $this->belongsTo(Candidate::class);
    }

    public function scopeChangedLastDay($query)
    {
        return $query->where('updated_at', '>', Carbon::now()->subDays(1))->where('status', '!=', 'opened');
    }
}
