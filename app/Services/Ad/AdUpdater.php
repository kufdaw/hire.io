<?php

namespace App\Services\Ad;

use App\Models\Ad;
use App\Models\Candidate;
use App\Models\User;
use App\Repositories\AdRepository;
use App\Repositories\ConversationRepository;
use Illuminate\Database\Eloquent\Model;

class AdUpdater
{
    private $adRepository;
    private $conversationRepository;

    /**
     * AdUpdater constructor.
     * @param AdRepository $adRepository
     * @param ConversationRepository $conversationRepository
     */
    public function __construct(
        AdRepository $adRepository,
        ConversationRepository $conversationRepository
    ) {
        $this->adRepository = $adRepository;
        $this->conversationRepository = $conversationRepository;
    }

    /**
     * @param Ad $ad
     * @param array $data
     * @return bool
     */
    public function update(Ad $ad, array $data): bool
    {
        if ($ad->status === 'discarded') {
            $data['status'] = 'unapproved';
        }

        return $this->adRepository->update($ad, $data);
    }

    public function proposeCandidate(Ad $ad, int $candidateId)
    {
        return $this->adRepository->proposeCandidate($ad, $candidateId);
    }

    public function removeCandidate(Ad $ad, int $candidateId)
    {
        return $this->adRepository->removeCandidate($ad, $candidateId);
    }

    public function setAdCandidatesStatusRead(User $user)
    {
        return $this->adRepository->setAdCandidatesStatusRead($user);
    }

    public function proceedCandidate(Ad $ad, Candidate $candidate)
    {
        $data = [
            'ad_id' => $ad->id,
            'candidate_id' => $candidate->id,
            'starter_user_id', auth()->user()->id,
        ];

        $this->conversationRepository->create($data, auth()->user()->id);

        return $this->adRepository->proceedCandidate($ad->id, $candidate->id);
    }

    public function rejectCandidate(Ad $ad, Candidate $candidate)
    {
        return $this->adRepository->rejectCandidate($ad, $candidate);
    }

    public function accept(Ad $ad)
    {
        return $this->adRepository->accept($ad);
    }

    public function discard(Ad $ad, string $discardReason)
    {
        return $this->adRepository->discard($ad, $discardReason);
    }

    public function deactivate(Ad $ad)
    {
        return $this->adRepository->deactivate($ad);
    }

    public function renew(Ad $ad)
    {
        return $this->adRepository->renew($ad);
    }
}
