<?php

namespace App\Services\Ad;

use App\Models\User;
use App\Repositories\AdRepository;
use Illuminate\Database\Eloquent\Collection;

class AdFetcher
{
    private $adRepository;

    /**
     * AdFetcher constructor.
     * @param AdRepository $adRepository
     */
    public function __construct(AdRepository $adRepository)
    {
        $this->adRepository = $adRepository;
    }

    public function find(int $adId)
    {
        return $this->adRepository->find($adId);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->adRepository->all();
    }

    public function getGroupedCandidateAdsByUser(int $userId)
    {
        return $this->adRepository->getGroupedCandidateAdsByUser($userId);
    }

    public function getUserAdsWhenHasCandidate(User $user)
    {
        return $this->adRepository->getUserAdsWhenHasCandidate($user);
    }

    public function active()
    {
        return $this->adRepository->active();
    }

    public function inactive()
    {
        return $this->adRepository->inactive();
    }

    public function moderation()
    {
        return $this->adRepository->moderation();
    }

    public function besidesModeration()
    {
        return $this->adRepository->besidesModeration();
    }

    public function search(array $params)
    {
        return $this->adRepository->search($params);
    }
}
