<?php

namespace App\Services\Ad;

use App\Models\Ad;
use App\Models\User;
use App\Repositories\AdRepository;

class AdCreator
{
    private $adRepository;

    /**
     * AdCreator constructor.
     * @param AdRepository $adRepository
     */
    public function __construct(AdRepository $adRepository)
    {
        $this->adRepository = $adRepository;
    }

    public function create(User $user, array $data)
    {
        return $this->adRepository->create($user, $data);
    }

    public function createFavoriteAd(int $adId, int $userId)
    {
        return $this->adRepository->createFavoriteAd($adId, $userId);
    }
}