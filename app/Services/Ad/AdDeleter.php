<?php

namespace App\Services\Ad;

use App\Models\Ad;
use App\Models\UserFavoriteAd;
use App\Repositories\AdRepository;

class AdDeleter
{
    private $adRepository;

    /**
     * AdDeleter constructor.
     * @param AdRepository $adRepository
     */
    public function __construct(AdRepository $adRepository)
    {
        $this->adRepository = $adRepository;
    }

    /**
     * @param Ad $ad
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Ad $ad): ?bool
    {
        return $this->adRepository->delete($ad);
    }

    /**
     * @param UserFavoriteAd $userFavoriteAd
     * @return bool|null
     * @throws \Exception
     */
    public function deleteFavoriteAd(UserFavoriteAd $userFavoriteAd): ?bool
    {
        return $this->adRepository->deleteFavoriteAd($userFavoriteAd);
    }
}
