<?php

namespace App\Services\Ad;

use App\Jobs\NewJobOffersEmailJob;
use App\Mail\NewCandidatesEmail;
use App\Mail\NewJobOffersEmail;
use App\Models\Ad;
use App\Models\Candidate;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class AdEmailSender
{
    public function sendUsersEmails(Collection $usersToSend, $daysSince)
    {
        foreach ($usersToSend as $user) {
            $regions = json_decode($user->notifications)->regions;
            $jobTypes = json_decode($user->notifications)->job_types;

            $ads = Ad::when($regions, function($query) use ($regions) {
                $query->whereIn('region', $regions);
            })->when($jobTypes, function ($query) use ($jobTypes) {
                $query->whereIn('job_type_id', $jobTypes);
            })->where('created_at', '>=', Carbon::now()->subDays($daysSince))->latest()->take(10)->get();

            if ($ads->count()) {
                dispatch(new NewJobOffersEmailJob($user->email, $ads));
            }
        }
    }
}
