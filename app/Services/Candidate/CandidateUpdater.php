<?php

namespace App\Services\Candidate;

use App\Models\Candidate;
use App\Repositories\CandidateRepository;

class CandidateUpdater
{
    protected $candidateRepository;
    protected $candidateParser;

    /**
     * CandidateUpdater constructor.
     * @param CandidateRepository $candidateRepository
     * @param CandidateParser $candidateParser
     */
    public function __construct(
        CandidateRepository $candidateRepository,
        CandidateParser $candidateParser
    ) {
        $this->candidateRepository = $candidateRepository;
        $this->candidateParser = $candidateParser;
    }

    /**
     * @param array $data
     * @param Candidate $candidate
     * @return bool
     */
    public function update(array $data, Candidate $candidate): bool
    {
        $candidate->syncTags($data['skills']);

        $this->candidateRepository->syncLanguages($candidate, $this->candidateParser->mapToLanguageIdAndLevel($data['languages']));

        if ($candidate->status === 'discarded') {
            $data['status'] = 'unapproved';
        }

        return $this->candidateRepository->update($data, $candidate);
    }

    /**
     * @param Candidate $candidate
     * @return bool
     */
    public function accept(Candidate $candidate): bool
    {
        return $this->candidateRepository->accept($candidate);
    }

    public function deactivate(Candidate $candidate)
    {
        return $this->candidateRepository->deactivate($candidate);
    }

    public function renew(Candidate $candidate)
    {
        return $this->candidateRepository->renew($candidate);
    }

    /**
     * @param Candidate $candidate
     * @param string $discardReason
     * @return bool
     */
    public function discard(Candidate $candidate, string $discardReason): bool
    {
        return $this->candidateRepository->discard($candidate, $discardReason);
    }
}
