<?php

namespace App\Services\Candidate;

class CandidateParser
{
    /**
     * @param array $languages
     * @return array
     */
    public function mapToLanguageIdAndLevel(array $languages): array
    {
        $parsedLanguages = [];

        foreach ($languages as $language) {
            if (isset($language['language_id']) && isset($language['language_level'])) {
                $parsedLanguages[] = [
                    'language_id' => $language['language_id'],
                    'language_level' => $language['language_level']
                ];
            }
        }

        return $parsedLanguages;
    }
}
