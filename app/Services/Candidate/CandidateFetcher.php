<?php

namespace App\Services\Candidate;

use App\Repositories\CandidateRepository;
use Illuminate\Support\Collection;

class CandidateFetcher
{
    private $candidateRepository;

    /**
     * CandidateFetcher constructor.
     * @param CandidateRepository $candidateRepository
     */
    public function __construct(CandidateRepository $candidateRepository)
    {
        $this->candidateRepository = $candidateRepository;
    }

    public function find(int $candidateId)
    {
        return $this->candidateRepository->find($candidateId);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->candidateRepository->all();
    }

    public function active()
    {
        return $this->candidateRepository->active();
    }

    public function inactive()
    {
        return $this->candidateRepository->inactive();
    }

    public function moderation()
    {
        return $this->candidateRepository->moderation();
    }

    public function besidesModeration()
    {
        return $this->candidateRepository->besidesModeration();
    }

    public function getUnproposedCandidates(Collection $candidates, Collection $adCandidates)
    {
        return $candidates->diff($adCandidates);
    }

    public function search(array $params)
    {
        return $this->candidateRepository->search($params);
    }
}
