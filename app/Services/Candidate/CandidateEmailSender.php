<?php

namespace App\Services\Candidate;

use App\Jobs\NewCandidatesEmailJob;
use App\Mail\NewCandidatesEmail;
use App\Models\Candidate;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class CandidateEmailSender
{
    public function __construct()
    {

    }

    public function sendUsersEmails(Collection $usersToSend, $daysSince)
    {
        foreach ($usersToSend as $user) {
            $regions = json_decode($user->notifications)->regions;
            $jobTypes = json_decode($user->notifications)->job_types;

            $candidates = Candidate::when($regions, function($query) use ($regions) {
                $query->whereIn('region', $regions);
            })->when($jobTypes, function ($query) use ($jobTypes) {
                $query->whereIn('job_type_id', $jobTypes);
            })->where('created_at', '>=', Carbon::now()->subDays($daysSince))->latest()->take(10)->get();

            if ($candidates->count()) {
                dispatch(new NewCandidatesEmailJob($user->email, $candidates));
            }
        }
    }
}
