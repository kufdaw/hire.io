<?php

namespace App\Services\Candidate;

use App\Models\User;
use App\Repositories\CandidateRepository;
use Illuminate\Database\Eloquent\Model;

class CandidateCreator
{
    protected $candidateRepository;
    protected $candidateParser;

    /**
     * CandidateCreator constructor.
     * @param CandidateRepository $candidateRepository
     * @param CandidateParser $candidateParser
     */
    public function __construct(
        CandidateRepository $candidateRepository,
        CandidateParser $candidateParser
    ) {
        $this->candidateRepository = $candidateRepository;
        $this->candidateParser = $candidateParser;
    }

    /**
     * @param array $data
     * @param User $user
     * @return Model
     */
    public function create(array $data, User $user): Model
    {
        $candidate = $this->candidateRepository->create($data, $user);
        $candidate->syncTags($data['skills']);
        $this->candidateRepository->syncLanguages($candidate, $this->candidateParser->mapToLanguageIdAndLevel($data['languages']));

        return $candidate;
    }

    public function createFavoriteCandidate(int $candidateId, int $userId)
    {
        return $this->candidateRepository->createFavoriteCandidate($candidateId, $userId);
    }
}
