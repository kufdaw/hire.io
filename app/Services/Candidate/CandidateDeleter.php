<?php

namespace App\Services\Candidate;

use App\Models\Ad;
use App\Models\Candidate;
use App\Models\UserFavoriteCandidate;
use App\Repositories\CandidateRepository;
use Illuminate\Support\Collection;

class CandidateDeleter
{
    private $candidateRepository;


    /**
     * CandidateDeleter constructor.
     * @param CandidateRepository $candidateRepository
     */
    public function __construct(CandidateRepository $candidateRepository)
    {
        $this->candidateRepository = $candidateRepository;
    }

    /**
     * @param Candidate $candidate
     * @return bool
     * @throws \Exception
     */
    public function delete(Candidate $candidate): bool
    {
        return $this->candidateRepository->delete($candidate);
    }

    /**
     * @param UserFavoriteCandidate $userFavoriteCandidate
     * @return bool|null
     * @throws \Exception
     */
    public function deleteFavoriteCandidate(UserFavoriteCandidate $userFavoriteCandidate)
    {
        return $this->candidateRepository->deleteFavoriteCandidate($userFavoriteCandidate);
    }
}
