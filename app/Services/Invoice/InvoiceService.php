<?php

namespace App\Services\Invoice;

use App\Models\User;
use Carbon\Carbon;

class InvoiceService
{

    public function handleCreateInvoice(User $user, array $data)
    {
        if ( !count($this->searchClient($data['tax_id'])) ) {
            $this->createClient($user, $data);
        }

        return $this->createInvoice($user, $data);
    }

    public function createInvoice(User $user, array $data)
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->post('https://kufdaw.fakturownia.pl/invoices.json',[
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'json' => [
                'api_token' => env('FAKTUROWNIA_TOKEN'),
                "invoice" => [
                    "kind" => "vat",
					"number" => null,
					"sell_date" => Carbon::now()->format('Y-m-d'),
					"issue_date" => Carbon::now()->format('Y-m-d'),
					"payment_to" => Carbon::now()->format('Y-m-d'),
					"buyer_tax_no" => $data['tax_id'],
					"buyer_name" => $data['company_name'],
					"buyer_email" => $user->email,
                    "buyer_city" => $data['company_city'],
                    "buyer_street" => $data['company_street'],
                    "buyer_post_code" => $data['company_postal_code'],
					"positions" =>  [
						[ "name" => $data['product_name'], "tax" => 23, "total_price_gross" => $data['price'], "quantity" => 1 ],
					]
				]
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }

    public function createClient(User $user, array $data)
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->post('https://kufdaw.fakturownia.pl/clients.json',[
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'json' => [
                'api_token' => env('FAKTUROWNIA_TOKEN'),
                'client' => [
                    'name' => $data['company_name'],
                    'tax_no' => $data['tax_id'],
                    'email' => $user->email,
                    'city' => $data['company_city'],
                    'post_code' => $data['company_postal_code'],
                    'street' => $data['company_street'],
                ]
            ]
        ]);

        return $response->getBody()->getContents();
    }

    public function searchClient($taxId)
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->get('https://kufdaw.fakturownia.pl/clients.json',[
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'json' => [
                'api_token' => env('FAKTUROWNIA_TOKEN'),
                'tax_no' => $taxId,
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }
}
