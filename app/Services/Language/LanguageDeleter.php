<?php

namespace App\Services\Language;

use App\Models\Language;
use App\Repositories\LanguageRepository;
use Illuminate\Database\Eloquent\Collection;

class LanguageDeleter
{
    private $languageRepository;

    /**
     * LanguageDeleter constructor.
     * @param LanguageRepository $languageRepository
     */
    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }


    /**
     * @param Language $language
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Language $language)
    {
        return $this->languageRepository->delete($language);
    }
}
