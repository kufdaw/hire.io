<?php

namespace App\Services\Language;

use App\Repositories\LanguageRepository;
use Illuminate\Database\Eloquent\Collection;

class LanguageFetcher
{
    private $languageRepository;

    /**
     * LanguageFetcher constructor.
     * @param LanguageRepository $languageRepository
     */
    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->languageRepository->all();
    }
}
