<?php

namespace App\Services\Language;

use App\Models\Language;
use App\Repositories\LanguageRepository;
use Illuminate\Database\Eloquent\Collection;

class LanguageCreator
{
    private $languageRepository;

    /**
     * LanguageCreator constructor.
     * @param LanguageRepository $languageRepository
     */
    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }


    /**
     * @param array $data
     * @return Language
     */
    public function create(array $data): Language
    {
        return $this->languageRepository->create($data);
    }
}
