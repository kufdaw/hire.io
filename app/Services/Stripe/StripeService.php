<?php

namespace App\Services\Stripe;

use Carbon\Carbon;

class StripeService
{
    public function getCustomer()
    {
        return auth()->user()->createOrGetStripeCustomer();
    }

    public function getCustomerPayments()
    {
        return auth()->user()->payments;
    }

    public function getAllPayments()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        return \Stripe\PaymentIntent::all();
    }

    public function getUserCandidatePayments()
    {
        $payments = $this->getCustomerPayments()->toArray();

        if (count($payments)) {
            return array_filter($payments, function ($payment) {
                return $payment['type'] === 'candidates' && $payment['status'] === 'succeeded';
            });
        }

        return [];
    }

    public function lastSubscriptionId()
    {
        $customer = $this->getCustomer();

        if (count($customer->subscriptions->data)) {
            $subscription = $customer->subscriptions->data[0];
            if ($subscription->status === 'active') {
                return $subscription->id;
            }
        }

        return false;
    }
}
