<?php

namespace App\Services\Country;

class CountryFetcher
{
    /**
     * @return array
     */
    public function all()
    {
        $countries = [];

        foreach (countries() as $code => $country) {
            $countries[] = [
                'name' => $country['name'],
                'code' => $code
            ];
        }

        return $countries;
    }
}
