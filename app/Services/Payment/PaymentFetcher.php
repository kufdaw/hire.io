<?php

namespace App\Services\Payment;

use App\Repositories\PaymentRepository;

class PaymentFetcher
{
    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    public function getPaginated($skip = 0, $take = 10)
    {
        return $this->paymentRepository->getPaginated($skip, $take);
    }
}
