<?php

namespace App\Services\Conversation;

use App\Models\Conversation;
use App\Repositories\ConversationRepository;

class ConversationUpdater
{
    private $conversationRepository;

    /**
     * ConversationUpdater constructor.
     * @param ConversationRepository $conversationRepository
     */
    public function __construct(ConversationRepository $conversationRepository)
    {
        $this->conversationRepository = $conversationRepository;
    }

    public function setStarterUserRead(Conversation $conversation)
    {
        return $this->conversationRepository->setStarterUserRead($conversation);
    }

    public function setOtherUserRead(Conversation $conversation)
    {
        return $this->conversationRepository->setOtherUserRead($conversation);
    }

    public function setAdminRead(Conversation $conversation)
    {
        return $this->conversationRepository->setAdminRead($conversation);
    }

    public function hide(Conversation $conversation, int $userId)
    {
        if ($userId === $conversation->starter_user_id) {
            return $this->conversationRepository->hideStarter($conversation);
        }

        return $this->conversationRepository->hideOther($conversation);
    }
}
