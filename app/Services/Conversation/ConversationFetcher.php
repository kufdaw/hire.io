<?php

namespace App\Services\Conversation;

use App\Models\Conversation;
use App\Models\User;
use App\Repositories\CandidateRepository;
use App\Repositories\ConversationRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ConversationFetcher
{
    private $conversationRepository;

    /**
     * ConversationFetcher constructor.
     * @param ConversationRepository $conversationRepository
     */
    public function __construct(ConversationRepository $conversationRepository)
    {
        $this->conversationRepository = $conversationRepository;
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function allUsers(User $user)
    {
        return $this->markHasStarted($this->conversationRepository->allUsers($user), $user);
    }

    /**
     * @return Conversation[]|Collection
     */
    public function all()
    {
        return $this->conversationRepository->all();
    }

    public function unreadMessagesCount(User $user)
    {
        $messagesAsStarter = $this->conversationRepository->allUsers($user)->where('starter_user_id', $user->id)->where('is_starter_read', 0)->count();
        $messagesAsOther = $this->conversationRepository->allUsers($user)->where('starter_user_id', '!=', $user->id)->where('is_other_read', 0)->count();

        return $messagesAsOther + $messagesAsStarter;
    }

    public function adminUnreadMessagesCount(User $user)
    {
        return $this->conversationRepository->all()->where('is_admin_read', 0)->count();
    }

    protected function markHasStarted(Collection $conversations, User $user)
    {
        $parsedConversations = [];

        foreach ($conversations as $conversation)
        {
            $conversation->canUserView = $this->canUserViewConversation($user, $conversation);

            if (!$conversation->canUserView) {
                $conversation = $conversation->toArray();
                $conversation['messages'][0]['user_id'] = null;
                $conversation['messages'][0]['text'] = 'Dummy message, you are not going to see it';
                $conversation['last_message'] = [$conversation['messages'][0]];
                $conversation['messages'] = [$conversation['messages'][0]];
                $conversation['starter_user_id'] = 0;
                $conversation['starter'] = (object) [
                    'id' => null,
                    'name' => 'Dummy name, you are not going to see it'
                ];
                $conversation['ad'] = null;
                $conversation['ad_id'] = null;
            }

            $parsedConversations[] = (object) $conversation;
        }

        return $parsedConversations;
    }

    public function canUserViewConversation(User $user, Conversation $conversation)
    {
        if ($conversation->canUserConversate($user->id)) {
            if ($user->hasRole('agency')) {
                if ($user->isPremiumActive()) {
                    return true;
                }

                if ($conversation->hasStarted()) {
                    return true;
                }

                return false;
            }

            return true;
        }

        return false;
    }
}
