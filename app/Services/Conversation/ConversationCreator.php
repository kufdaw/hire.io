<?php

namespace App\Services\Conversation;

use App\Models\Conversation;
use App\Models\User;
use App\Repositories\AdRepository;
use App\Repositories\CandidateRepository;
use App\Repositories\ConversationRepository;
use Illuminate\Database\Eloquent\Model;

class ConversationCreator
{
    protected $conversationRepository;
    protected $adRepository;

    public function __construct(
        ConversationRepository $conversationRepository,
        AdRepository $adRepository
    ) {
        $this->conversationRepository = $conversationRepository;
        $this->adRepository = $adRepository;
    }

    public function create(array $data, int $starterUserId)
    {
        $conversation = $this->conversationRepository->create($data, $starterUserId);

        if (isset($data['ad_id'])) {
            $this->adRepository->proceedCandidate($data['ad_id'], $data['candidate_id']);
        }
        $this->conversationRepository->createMessage($conversation, $data['text'], $starterUserId);

        return $conversation;
    }

    public function createMessage(Conversation $conversation, string $message, int $userId)
    {
        if ($userId === $conversation->starter_user_id) {
            $this->conversationRepository->setOtherUserUnread($conversation);
        } else {
            $this->conversationRepository->setStarterUserUnread($conversation);
        }

        return $this->conversationRepository->createMessage($conversation, $message, $userId);
    }
}
