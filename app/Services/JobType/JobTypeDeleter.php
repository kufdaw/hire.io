<?php

namespace App\Services\JobType;

use App\Models\JobType;
use App\Models\Language;
use App\Repositories\JobTypeRepository;
use Illuminate\Database\Eloquent\Collection;

class JobTypeDeleter
{
    private $jobTypeRepository;

    /**
     * JobTypeDeleter constructor.
     * @param JobTypeRepository $jobTypeRepository
     */
    public function __construct(JobTypeRepository $jobTypeRepository)
    {
        $this->jobTypeRepository = $jobTypeRepository;
    }

    /**
     * @param JobType $jobType
     * @return bool|null
     * @throws \Exception
     */
    public function delete(JobType $jobType)
    {
        return $this->jobTypeRepository->delete($jobType);
    }
}
