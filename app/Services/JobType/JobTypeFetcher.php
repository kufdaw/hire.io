<?php

namespace App\Services\JobType;

use App\Repositories\JobTypeRepository;
use Illuminate\Database\Eloquent\Collection;

class JobTypeFetcher
{
    private $jobTypeRepository;

    /**
     * JobTypeFetcher constructor.
     * @param JobTypeRepository $jobTypeRepository
     */
    public function __construct(JobTypeRepository $jobTypeRepository)
    {
        $this->jobTypeRepository = $jobTypeRepository;
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->jobTypeRepository->all();
    }
}