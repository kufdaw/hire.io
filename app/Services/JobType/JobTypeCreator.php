<?php

namespace App\Services\JobType;

use App\Models\JobType;
use App\Repositories\JobTypeRepository;
use Illuminate\Database\Eloquent\Collection;

class JobTypeCreator
{
    private $jobTypeRepository;

    /**
     * JobTypeFetcher constructor.
     * @param JobTypeRepository $jobTypeRepository
     */
    public function __construct(JobTypeRepository $jobTypeRepository)
    {
        $this->jobTypeRepository = $jobTypeRepository;
    }

    /**
     * @param array $data
     * @return JobType
     */
    public function create(array $data): JobType
    {
        return $this->jobTypeRepository->create($data);
    }
}
