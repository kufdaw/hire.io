<?php

namespace App\Services\User;

use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Collection;

class UserFetcher
{
    private $userRepository;

    /**
     * UserFetcher constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->userRepository->all();
    }

    public function getPaginatedPremiumAgencies($skip = 0, $take = 10)
    {
        return $this->userRepository->getPaginatedPremiumAgencies($skip, $take);
    }
}
