<?php

namespace App\Services\User;

use App\Repositories\UserRepository;

class UserCreator
{
    private $userRepository;

    /**
     * UserCreator constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

}