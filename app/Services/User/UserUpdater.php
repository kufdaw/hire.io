<?php

namespace App\Services\User;

use App\Models\User;
use App\Repositories\UserRepository;

class UserUpdater
{
    private $userRepository;

    /**
     * UserUpdater constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @param array $data
     * @return mixed
     */
    public function update(User $user, array $data)
    {
        return $this->userRepository->update($user, $data);
    }

    /**
     * @param User $user
     * @param array $data
     * @return bool
     */
    public function updatePassword(User $user, array $data)
    {
        return $this->userRepository->updatePassword($user, $data);
    }

    /**
     * @param User $user
     * @param array $data
     * @return bool
     */
    public function updateNotifications(User $user, array $data)
    {
        return $this->userRepository->updateNotifications($user, $data);
    }

    /**
     * @param User $user
     * @param array $data
     * @param $logoPath
     * @return bool
     */
    public function updateCard(User $user, array $data, $logoPath)
    {
        return $this->userRepository->updateCard($user, $data, $logoPath);
    }

    /**
     * @param User $user
     * @param array $data
     * @return bool
     */
    public function patch(User $user, array $data)
    {
        return $this->userRepository->patch($user, $data);
    }
}
