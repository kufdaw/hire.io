<?php

namespace App\Services\User;

use App\Repositories\UserRepository;

class UserDeleter
{
    private $userRepository;

    /**
     * UserDeleter constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }
}