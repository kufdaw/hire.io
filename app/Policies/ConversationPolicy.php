<?php

namespace App\Policies;

use App\Models\Conversation;
use App\Models\User;
use App\Services\Conversation\ConversationFetcher;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConversationPolicy
{
    use HandlesAuthorization;

    protected $conversationFetcher;

    /**
     * Create a new policy instance.
     *
     * @param ConversationFetcher $conversationFetcher
     */
    public function __construct(ConversationFetcher $conversationFetcher)
    {
        $this->conversationFetcher = $conversationFetcher;
    }

    public function view(User $user, Conversation $conversation)
    {
        return $this->conversationFetcher->canUserViewConversation($user, $conversation);
    }
}
