<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('send:candidate-expired')->cron('0 */4 * * *');
        $schedule->command('send:ad-expired')->cron('0 */4 * * *');
        $schedule->command('send:message-received')->cron('0 */1 * * *');
        $schedule->command('send:offer-status-changed')->daily();
        $schedule->command('send:jobs-daily')->daily();
        $schedule->command('send:jobs-weekly')->weekly();
        $schedule->command('send:jobs-monthly')->monthly();
        $schedule->command('send:candidates-daily')->daily();
        $schedule->command('send:candidates-weekly')->weekly();
        $schedule->command('send:candidates-monthly')->monthly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
