<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\Ad\AdEmailSender;
use Illuminate\Console\Command;

class SendNewJobsWeekly extends Command
{
    protected $adEmailSender;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:jobs-weekly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param AdEmailSender $adEmailSender
     */
    public function __construct(AdEmailSender $adEmailSender)
    {
        $this->adEmailSender = $adEmailSender;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $usersToSend = User::whereHas('roles', function ($query) {
            $query->where('name', 'agency');
        })->where('notifications->frequency', 7)
        ->where('notifications->new_job_offers', true)->get();

        $this->adEmailSender->sendUsersEmails($usersToSend, 7);
    }
}
