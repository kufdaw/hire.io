<?php

namespace App\Console\Commands;

use App\Jobs\CandidateExpiredEmailJob;
use App\Jobs\OfferExpiredEmailJob;
use App\Models\Candidate;
use Illuminate\Console\Command;

class SendCandidateExpiredEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:candidate-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails to users whenever candidate status expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $candidates = Candidate::expiredHoursAgo(4)->get();
        $link = route('dashboard-candidates');

        foreach ($candidates->groupBy('owner_id') as $userId => $candidate) {
            if (json_encode($candidate->owner->notifications)->candidate_expired) {
                dispatch(new CandidateExpiredEmailJob($candidate[0]->owner->email, $link));
            }
        }
    }
}
