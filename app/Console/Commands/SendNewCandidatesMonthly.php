<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\Candidate\CandidateEmailSender;
use Illuminate\Console\Command;

class SendNewCandidatesMonthly extends Command
{
    protected $candidateEmailSender;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:candidates-monthly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param CandidateEmailSender $candidateEmailSender
     */
    public function __construct(CandidateEmailSender $candidateEmailSender)
    {
        $this->candidateEmailSender = $candidateEmailSender;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $usersToSend = User::whereHas('roles', function ($query) {
            $query->where('name', 'company');
        })->where('notifications->frequency', 30)
        ->where('notifications->new_candidates', true)->get();

        $this->candidateEmailSender->sendUsersEmails($usersToSend, 30);
    }
}
