<?php

namespace App\Console\Commands;

use App\Jobs\OfferExpiredEmailJob;
use App\Models\Ad;
use Illuminate\Console\Command;

class SendAdExpiredEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:ad-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails to users whenever offer expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ads = Ad::expiredHoursAgo(4)->get()->groupBy('owner_id');
        $link = route('dashboard-ads');

        foreach ($ads->groupBy('owner_id') as $userId => $ad) {
            if (json_encode($ad->owner->notifications)->offer_expired) {
                dispatch(new OfferExpiredEmailJob($ad[0]->owner->email, $link));
            }
        }
    }
}
