<?php

namespace App\Console\Commands;

use App\Jobs\NewMessageEmailJob;
use App\Models\Message;
use Illuminate\Console\Command;

class SendMessageReceivedEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:message-received';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder for every 4 hours when message received';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $messages = Message::fromLastHours(1)->get();
        $usersToSend = [];
        $link = route('dashboard-messages');

        foreach ($messages as $message) {
            if ($message->conversation->starter_user_id === $message->user_id) {
                $usersToSend[] = $message->conversation->candidate->owner;
            } else {
                $usersToSend[] = $message->conversation->starter;
            }
        }

        foreach (array_unique($usersToSend) as $user) {
            if (json_encode($user->notifications)->new_message) {
                dispatch(new NewMessageEmailJob($user->email, $link));
            }
        }
    }
}
