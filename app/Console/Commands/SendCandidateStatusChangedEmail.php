<?php

namespace App\Console\Commands;

use App\Jobs\CandidateStatusChangedEmailJob;
use App\Models\AdCandidate;
use Illuminate\Console\Command;

class SendCandidateStatusChangedEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:offer-status-changed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send remiders to users once a day when candidate status changed.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $recruitments = AdCandidate::changedLastDay()->get()->groupBy('candidate.owner_id');

        $link = route('dashboard-candidates');

        foreach ($recruitments as $userId => $recruitment) {
            $candidate = $recruitment[0]->candidate;
            $candidateOwner = $candidate->owner;

            if (json_encode($candidateOwner->notifications)->candidate_status_changed) {
                dispatch(new CandidateStatusChangedEmailJob($candidateOwner->email, $candidate->title, $link));
            }
        }

    }
}
