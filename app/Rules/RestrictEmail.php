<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RestrictEmail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $bannedEmails = ['wp.pl', 'poczta.onet.pl', 'op.pl', 'vp.pl', 'o2.pl', 'tlen.pl', 'go2.pl', 'interia.pl', 'poczta.fm', 'gmail.com', 'gazeta.pl', 'yahoo.com', 'hotmail.com', '10MinuteMail.com', 'migmail.pl', 'koszmail.pl', 'pamBox.us', 'MyTrashMail.com', 'Mailinator.com', 'getairmail.com', 'mytrashmail.com', 'poczter.eu'];

        if (request()->get('role') === 'company') {
            foreach ($bannedEmails as $email) {
                if (strpos($value, $email)) {
                    return false;
                }
            }
        }

        return true;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You need to use company\'s email';
    }
}
