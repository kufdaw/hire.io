const mix = require('laravel-mix')
const path = require('path')
const tailwindcss = require('tailwindcss')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

mix.extend('vuetify', new class {
    webpackConfig (config) {
        config.plugins.push(new VuetifyLoaderPlugin())
    }
})

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .vuetify()
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('tailwind.config.js') ],
    })
    .webpackConfig({
        output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.runtime.esm.js',
                '@': path.resolve('resources/js'),
                '@images': path.resolve('resources/images'),
                '@storage': path.resolve('storage'),
            },
        },
    })
    .babelConfig({
        plugins: [
            '@babel/plugin-syntax-dynamic-import',
        ]
    })
